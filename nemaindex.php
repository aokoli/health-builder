<!--Test Alex. Works for us all! Great job-->

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="food, healthy, workout, meat">
    <meta name="author" content="Nema Nemati & Tyler Bobella">
    <link rel="shortcut icon" href="images/favicon.ico">

    <title>GoHealthBuilder</title>

    <!-- Bootstrap core CSS -->
    <link href="dist/css/bootstrap.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="jumbotron.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="../../assets/js/html5shiv.js"></script>
      <script src="../../assets/js/respond.min.js"></script>
    <![endif]-->
  </head>

  <style>
  #tab-container {
    overflow:hidden;
}
</style>

  <body>

    <div class="navbar navbar-inverse navbar-fixed-top">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="http://gohealthbuilder.com/">Health Builder</a>
        </div>
        <div class="navbar-collapse collapse">
          <ul class="nav navbar-nav">
            <li class="active"><a href="http://gohealthbuilder.com/">Home</a></li>
            <li><a href="http://gohealthbuilder.com/bootstrap-3.0.0/examples/starter-template/about.html">About</a></li>
            <li><a href="http://gohealthbuilder.com/bootstrap-3.0.0/examples/starter-template/contact.html">Contact</a></li>
             
          </ul>
          <form class="navbar-form navbar-right">
            <div class="form-group">
              <input type="text" placeholder="Email" class="form-control">
            </div>
            <div class="form-group">
              <input type="password" placeholder="Password" class="form-control">
            </div>
            <button type="submit" class="btn btn-success">Sign in</button>
          </form>
        </div><!--/.navbar-collapse -->
      </div>
    </div>

    <!-- Main jumbotron for a primary marketing message or call to action -->
    <div class="jumbotron">
      <div class="container">
        <center><h1>Health Builder</h1></center>
        <center><p>Don't bother handling you're health, we'll do it for you!</p></center>
          
              <div class="accordion" id="monogram-acc">
                  <div class="accordion-group">
                      <div class="accordion-heading">
                          <center><div class="btn btn-primary btn-lg" data-toggle="collapse" data-parent="#monogram-acc" href="#Animals">BUILD YOUR SCHEDULE</div></center>
                      </div>
                      <center><div class="accordion-body collapse" id="Animals"><br>
                      <div class="container">
    <div class="container">
  <div class="row">
    <div class="span12">

     <div class="panel-group" id="accordion">
  <div class="panel panel-default">
    <div class="panel-heading">
      <h4 class="panel-title">
        <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
          You'll be able to sign up below soon! Move on to choose your protein for now!
        </a>
      </h4>
    </div>
    

    <div id="collapseOne" class="panel-collapse collapse in">
      <div class="panel-body">
          <div class="btn-group" data-toggle="buttons">
                <form>
                <div class="col-lg-4">
                   First Name: <input type="text" class="form-control" name="firstname" placeholder="First Name">
                </div>
                <div class="col-lg-4">
                   Last Name: <input type="text" class="form-control" name="lastname" placeholder="Last Name">
                </div>
                <div class="col-lg-4">
                   Email: <input type="email" class="form-control" name="email" placeholder="Email">
                </div>
          </div>
      </div>
 </div>



  <div class="panel panel-default">
    <div class="panel-heading">
      <h4 class="panel-title">
        <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">
          Choose your favorite protein!
        </a>
      </h4>
    </div>
    <div id="collapseTwo" class="panel-collapse collapse">
      <div class="panel-body">
                <label class="btn btn-primary">
                   
                  <input id="search" type="checkbox" name="c1" id="c1" onclick="showMe('Pro1')">Beef

                   <!-- <input type="checkbox" id="beef" value="Steak"/> Beef -->
                   
                                                    </label> 
                <label class="btn btn-primary" id="lamb">
                    <input id="search" type="checkbox" name="c2" id="c2" onclick="showMee('Lamb')">Lamb
                                                    </label>
                <label class="btn btn-primary" id="seafood">
                    <input id="search" type="checkbox" name="c3" id="c3" onclick="showSea('Seafood')">Seafood
                                                        </label>

                 <label class="btn btn-primary">
                      <input id="search" type="checkbox" name="c4" id="c4" onclick="showChi('Chicken')">Chicken
            </label> 
          </div>
      </div>
    </div>
  </div>

   



   <div class="panel panel-default">
    <div class="panel-heading">
      <h4 class="panel-title">
        <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseT">
          Would you like some veggies to compliment your meals?
        </a>
      </h4>
    </div>
    <div id="collapseT" class="panel-collapse collapse">
      <div class="panel-body">

                <label class="btn btn-primary">
                    <input id="search" type="checkbox" name="b1" id="b1" onclick="showVeg('Vegetable')">Yes
                </label>

                <label class="btn btn-primary">
                    <input type="checkbox" id="veggieNo"> 
                    No
                </label>

                
          </div>
      </div>
    </div>
  <div class="panel panel-default">
    <div class="panel-heading">
      <h4 class="panel-title">
        <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseThree">
          Add some sweetness to your meal plan! (fruits, we mean fruits)
        </a>
      </h4>
    </div>
    <div id="collapseThree" class="panel-collapse collapse">
      <div class="panel-body">
              <p> When selecting yes you will be given an assortment of fruits to have with your meal or as a snack throughout the day.</p>
          <label class="btn btn-primary">
                     <input id="search" type="checkbox" name="b2" id="b2" onclick="showFruit('Fruit')"><p>Yes! Fruit me up!</p>
          </label>

           <label class="btn btn-primary">
                     <input type="checkbox" id="noFruit"> 
                     <p>No thanks, I'll pass on some delicious sweetness.</p>
            </label>     

        
      </div>
    </div>
  </div>
<div class="panel panel-default">
    <div class="panel-heading">
      <h4 class="panel-title">
        <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseFour">
          The best part: your workout! Choose the type of work out you are looking for!
        </a>
      </h4>
    </div>
    <div id="collapseFour" class="panel-collapse collapse">
      <div class="panel-body">
        


                <label class="btn btn-primary">
                    <input type="radio" id="cario" name="workout"> 
                    Cardio (time to burn some calories!)
                </label>

                <label class="btn btn-primary">
                    <input type="radio" id="lean" name="workout"> 
                    Want to look like Ashton Kutcher? Time to build some lean muscle!
                </label>

                <label class="btn btn-primary">
                     <input type="radio" id="bulk" name="workout"> 
                     Diggin' the Hulk Hogan look? Let's bulk you up!
                </label>

                <label class="btn btn-primary">
                     <input type="radio" id="xfit" name="workout"> 
                     Like variety and a little of everything? Crossfit is for you!
                </label>
          </div>

          <div>
              <!-- <label class="btn btn-primary">
                      <!-- <input type="submit">
                </label>  -->
          </div>

        </form>


      </div>
    </div>
  </div>


  <div class="panel panel-default">
    <div class="panel-heading">
      <h4 class="panel-title">
        <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseSix">
          Your Schedule!
        </a>
      </h4>
    </div>
<div id="collapseSix" class="panel-collapse collapse">
      <div class="panel-body">
          <div class="btn-group" data-toggle="buttons">
                
                <div class="col-lg-4">

                  <script type="text/javascript">
                     function showMe (box) {
        
        var chboxs = document.getElementsByName("c1");
        
        var vis = "none";
        for(var i=0;i<chboxs.length;i++) { 
            if(chboxs[i].checked){
              
             vis = "block";
                break;
            }
        }
        document.getElementById(box).style.display = vis;
        document.getElementsByName("Pro1")[0].style.display = vis;
        document.getElementsByName("Pro1")[1].style.display = vis;
        document.getElementsByName("Pro1")[2].style.display = vis;
        document.getElementsByName("Pro1")[3].style.display = vis;
        document.getElementsByName("Pro1")[4].style.display = vis;


    }


    function showMee (box) {
        
        var ch = document.getElementsByName("c2");
        
        var vi = "none";
        for(var i=0;i<ch.length;i++) { 
            if(ch[i].checked){
              
             vi = "block";
                break;
            }
        }
        document.getElementById(box).style.display = vi;
        document.getElementsByName("Pro2")[0].style.display = vi;
        document.getElementsByName("Pro2")[1].style.display = vi;
        document.getElementsByName("Pro2")[2].style.display = vi;
        document.getElementsByName("Pro2")[3].style.display = vi;
        document.getElementsByName("Pro2")[4].style.display = vi;
    
    
    }


    function showSea (box) {
        
        var a = document.getElementsByName("c3");
        
        var b = "none";
        for(var i=0;i<a.length;i++) { 
            if(a[i].checked){
              
             b = "block";
                break;
            }
        }
        document.getElementById(box).style.display = b;
        document.getElementsByName("Pro3")[0].style.display = b;
        document.getElementsByName("Pro3")[1].style.display = b;
        document.getElementsByName("Pro3")[2].style.display = b;
        document.getElementsByName("Pro3")[3].style.display = b;
        document.getElementsByName("Pro3")[4].style.display = b;
    
    
    }


    function showChi (box) {
        
        var c = document.getElementsByName("c4");
        
        var d = "none";
        for(var i=0;i<c.length;i++) { 
            if(c[i].checked){
              
             d = "block";
                break;
            }
        }
        document.getElementById(box).style.display = d;
        document.getElementsByName("Pro4")[0].style.display = d;
        document.getElementsByName("Pro4")[1].style.display = d;
        document.getElementsByName("Pro4")[2].style.display = d;
        document.getElementsByName("Pro4")[3].style.display = d;
        document.getElementsByName("Pro4")[4].style.display = d;
    
    
    }



    function showVeg (box) {
        
        var e = document.getElementsByName("b1");
        
        var f = "none";
        for(var i=0;i<e.length;i++) { 
            if(e[i].checked){
              
             f = "block";
                break;
            }
        }
        document.getElementById(box).style.display = f;
        document.getElementsByName("Veg")[0].style.display = f;
        document.getElementsByName("Veg")[1].style.display = f;
        document.getElementsByName("Veg")[2].style.display = f;
        document.getElementsByName("Veg")[3].style.display = f;
        document.getElementsByName("Veg")[4].style.display = f;
    
    
    }




    function showFruit (box) {
        
        var g = document.getElementsByName("b2");
        
        var h = "none";
        for(var i=0;i<g.length;i++) { 
            if(g[i].checked){
              
             h = "block";
                break;
            }
        }
        document.getElementById(box).style.display = h;
        document.getElementsByName("fruit")[0].style.display = h;
        document.getElementsByName("fruit")[1].style.display = h;
        document.getElementsByName("fruit")[2].style.display = h;
        document.getElementsByName("fruit")[3].style.display = h;
        document.getElementsByName("fruit")[4].style.display = h;
    
    
    }
    </script>



    

    
    <center><table class="table table-hover" border="5">
                  <tr>
                    <th>Monday</th>
                    <th>Tuesday</th>
                    <th>Wednesday</th>
                    <th>Thursday</th>
                    <th>Friday</th>
                  </tr>
                  <tr>
                    <td><div id="Pro1" name="Pro1" style="display:none">
                        <?php
$con=mysqli_connect("localhost","gohealth_test","iaminlove","gohealth_test");
// Check connection
if (mysqli_connect_errno())
  {
  echo "Failed to connect to MySQL: " . mysqli_connect_error();
  }

$result = mysqli_query($con,"SELECT * FROM recipe WHERE `recipeType` LIKE 'Beef' ORDER BY RAND() LIMIT 1");

while($row = mysqli_fetch_array($result))
  {
  echo $row['recipeName'] /*. " " . $row['recipeType']*/;
  echo "<br>";
  }
?></div>
                     <div id="Lamb" name="Pro2" style="display:none">

                        <?php
$con=mysqli_connect("localhost","gohealth_test","iaminlove","gohealth_test");
// Check connection
if (mysqli_connect_errno())
  {
  echo "Failed to connect to MySQL: " . mysqli_connect_error();
  }

$result = mysqli_query($con,"SELECT * FROM recipe WHERE `recipeType` LIKE 'Lamb' ORDER BY RAND() LIMIT 1");

while($row = mysqli_fetch_array($result))
  {
  echo $row['recipeName'] /*. " " . $row['recipeType']*/;
  echo "<br>";
  }
?>

                     </div>
                            <div id="Seafood" name="Pro3" style="display:none">
                              <?php
$con=mysqli_connect("localhost","gohealth_test","iaminlove","gohealth_test");
// Check connection
if (mysqli_connect_errno())
  {
  echo "Failed to connect to MySQL: " . mysqli_connect_error();
  }

$result = mysqli_query($con,"SELECT * FROM recipe WHERE `recipeType` LIKE 'Seafood' ORDER BY RAND() LIMIT 1");

while($row = mysqli_fetch_array($result))
  {
  echo "<br>";
  echo "n" . $row['recipeName'] /*. " " . $row['recipeType']*/;
  echo "<br>";
  }
?>


                            </div>
                                    <div id="Chicken" name="Pro4" style="display:none">

                                      <?php
$con=mysqli_connect("localhost","gohealth_test","iaminlove","gohealth_test");
// Check connection
if (mysqli_connect_errno())
  {
  echo "Failed to connect to MySQL: " . mysqli_connect_error();
  }

$result = mysqli_query($con,"SELECT * FROM recipe WHERE `recipeType` LIKE 'Chicken' ORDER BY RAND() LIMIT 1");

while($row = mysqli_fetch_array($result))
  {
  echo $row['recipeName'] /*. " " . $row['recipeType']*/;
  echo "<br>";
  }
?>


                                    </div>


                    </td>
                    <td><div id="Pro2" name="Pro1" style="display:none">
                      <?php
$con=mysqli_connect("localhost","gohealth_test","iaminlove","gohealth_test");
// Check connection
if (mysqli_connect_errno())
  {
  echo "Failed to connect to MySQL: " . mysqli_connect_error();
  }

$result = mysqli_query($con,"SELECT * FROM recipe WHERE `recipeType` LIKE 'Beef' ORDER BY RAND() LIMIT 1");

while($row = mysqli_fetch_array($result))
  {
  echo $row['recipeName'] /*. " " . $row['recipeType']*/;
  echo "<br>";
  }
?>
</div>
                                    <div id="Lamb" name="Pro2" style="display:none"> 
                                        <?php
$con=mysqli_connect("localhost","gohealth_test","iaminlove","gohealth_test");
// Check connection
if (mysqli_connect_errno())
  {
  echo "Failed to connect to MySQL: " . mysqli_connect_error();
  }

$result = mysqli_query($con,"SELECT * FROM recipe WHERE `recipeType` LIKE 'Lamb' ORDER BY RAND() LIMIT 1");

while($row = mysqli_fetch_array($result))
  {
  echo $row['recipeName'] /*. " " . $row['recipeType']*/;
  echo "<br>";
  }
?>


                                      </div>



                                      <div id="Seafood" name="Pro3" style="display:none">

                                        <?php
$con=mysqli_connect("localhost","gohealth_test","iaminlove","gohealth_test");
// Check connection
if (mysqli_connect_errno())
  {
  echo "Failed to connect to MySQL: " . mysqli_connect_error();
  }

$result = mysqli_query($con,"SELECT * FROM recipe WHERE `recipeType` LIKE 'Seafood' ORDER BY RAND() LIMIT 1");

while($row = mysqli_fetch_array($result))
  {
  echo $row['recipeName'] /*. " " . $row['recipeType']*/;
  echo "<br>";
  }
?>

                            </div>

                            <div id="Chicken" name="Pro4" style="display:none">
                              <?php
$con=mysqli_connect("localhost","gohealth_test","iaminlove","gohealth_test");
// Check connection
if (mysqli_connect_errno())
  {
  echo "Failed to connect to MySQL: " . mysqli_connect_error();
  }

$result = mysqli_query($con,"SELECT * FROM recipe WHERE `recipeType` LIKE 'Chicken' ORDER BY RAND() LIMIT 1");

while($row = mysqli_fetch_array($result))
  {
  echo $row['recipeName'] /*. " " . $row['recipeType']*/;
  echo "<br>";
  }
?>



                                    </div>

</td>
                    <td><div id="Pro3" name="Pro1" style="display:none"><?php
$con=mysqli_connect("localhost","gohealth_test","iaminlove","gohealth_test");
// Check connection
if (mysqli_connect_errno())
  {
  echo "Failed to connect to MySQL: " . mysqli_connect_error();
  }

$result = mysqli_query($con,"SELECT * FROM recipe WHERE `recipeType` LIKE 'Beef' ORDER BY RAND() LIMIT 1");

while($row = mysqli_fetch_array($result))
  {
  echo $row['recipeName'] /*. " " . $row['recipeType']*/;
  echo "<br>";
  }
?>
</div>
                          <div id="Lamb" name="Pro2" style="display:none">
                              <?php
$con=mysqli_connect("localhost","gohealth_test","iaminlove","gohealth_test");
                    // Check connection
  if (mysqli_connect_errno())
  {
  echo "Failed to connect to MySQL: " . mysqli_connect_error();
  }

$result = mysqli_query($con,"SELECT * FROM recipe WHERE `recipeType` LIKE 'Lamb' ORDER BY RAND() LIMIT 1");

while($row = mysqli_fetch_array($result))
  {
  echo $row['recipeName'] /*. " " . $row['recipeType']*/;
  echo "<br>";
  }
?>


                          </div>


                          <div id="Seafood" name="Pro3" style="display:none">
                            <?php
$con=mysqli_connect("localhost","gohealth_test","iaminlove","gohealth_test");
// Check connection
if (mysqli_connect_errno())
  {
  echo "Failed to connect to MySQL: " . mysqli_connect_error();
  }

$result = mysqli_query($con,"SELECT * FROM recipe WHERE `recipeType` LIKE 'Seafood' ORDER BY RAND() LIMIT 1");

while($row = mysqli_fetch_array($result))
  {
  echo $row['recipeName'] /*. " " . $row['recipeType']*/;
  echo "<br>";
  }
?>


                            </div>

                            <div id="Chicken" name="Pro4" style="display:none">
                              <?php
$con=mysqli_connect("localhost","gohealth_test","iaminlove","gohealth_test");
// Check connection
if (mysqli_connect_errno())
  {
  echo "Failed to connect to MySQL: " . mysqli_connect_error();
  }

$result = mysqli_query($con,"SELECT * FROM recipe WHERE `recipeType` LIKE 'Chicken' ORDER BY RAND() LIMIT 1");

while($row = mysqli_fetch_array($result))
  {
  echo $row['recipeName'] /*. " " . $row['recipeType']*/;
  echo "<br>";
  }
?>



                                    </div>




</td>
                    <td><div id="Pro4" name="Pro1" style="display:none"><?php
$con=mysqli_connect("localhost","gohealth_test","iaminlove","gohealth_test");
// Check connection
if (mysqli_connect_errno())
  {
  echo "Failed to connect to MySQL: " . mysqli_connect_error();
  }

$result = mysqli_query($con,"SELECT * FROM recipe WHERE `recipeType` LIKE 'Beef' ORDER BY RAND() LIMIT 1");

while($row = mysqli_fetch_array($result))
  {
  echo $row['recipeName'] /*. " " . $row['recipeType']*/;
  echo "<br>";
  }
?></div>
                                  <div id="Lamb" name="Pro2" style="display:none">
                                    <?php
$con=mysqli_connect("localhost","gohealth_test","iaminlove","gohealth_test");
// Check connection
if (mysqli_connect_errno())
  {
  echo "Failed to connect to MySQL: " . mysqli_connect_error();
  }

$result = mysqli_query($con,"SELECT * FROM recipe WHERE `recipeType` LIKE 'Lamb' ORDER BY RAND() LIMIT 1");

while($row = mysqli_fetch_array($result))
  {
  echo $row['recipeName'] /*. " " . $row['recipeType']*/;
  echo "<br>";
  }
?>


                                  </div>


                                  <div id="Seafood" name="Pro3" style="display:none">
                                    <?php
$con=mysqli_connect("localhost","gohealth_test","iaminlove","gohealth_test");
// Check connection
if (mysqli_connect_errno())
  {
  echo "Failed to connect to MySQL: " . mysqli_connect_error();
  }

$result = mysqli_query($con,"SELECT * FROM recipe WHERE `recipeType` LIKE 'Seafood' ORDER BY RAND() LIMIT 1");

while($row = mysqli_fetch_array($result))
  {
  echo $row['recipeName'] /*. " " . $row['recipeType']*/;
  echo "<br>";
  }
?>


                            </div>


                            <div id="Chicken" name="Pro4" style="display:none">
                              <?php
$con=mysqli_connect("localhost","gohealth_test","iaminlove","gohealth_test");
// Check connection
if (mysqli_connect_errno())
  {
  echo "Failed to connect to MySQL: " . mysqli_connect_error();
  }

$result = mysqli_query($con,"SELECT * FROM recipe WHERE `recipeType` LIKE 'Chicken' ORDER BY RAND() LIMIT 1");

while($row = mysqli_fetch_array($result))
  {
  echo $row['recipeName'] /*. " " . $row['recipeType']*/;
  echo "<br>";
  }
?>



                                    </div>



</td>
                    <td><div id="Pro5" name="Pro1" style="display:none"><?php
$con=mysqli_connect("localhost","gohealth_test","iaminlove","gohealth_test");
// Check connection
if (mysqli_connect_errno())
  {
  echo "Failed to connect to MySQL: " . mysqli_connect_error();
  }

$result = mysqli_query($con,"SELECT * FROM recipe WHERE `recipeType` LIKE 'Beef' ORDER BY RAND() LIMIT 1");

while($row = mysqli_fetch_array($result))
  {
  echo $row['recipeName'] /*. " " . $row['recipeType']*/;
  echo "<br>";
  }
?></div>

                                  <div id="Lamb" name="Pro2" style="display:none">

                                    <?php
$con=mysqli_connect("localhost","gohealth_test","iaminlove","gohealth_test");
// Check connection
if (mysqli_connect_errno())
  {
  echo "Failed to connect to MySQL: " . mysqli_connect_error();
  }

$result = mysqli_query($con,"SELECT * FROM recipe WHERE `recipeType` LIKE 'Lamb' ORDER BY RAND() LIMIT 1");

while($row = mysqli_fetch_array($result))
  {
  echo $row['recipeName'] /*. " " . $row['recipeType']*/;
  echo "<br>";
  }
?>


                                  </div>



                                  <div id="Seafood" name="Pro3" style="display:none">
                                    <?php
$con=mysqli_connect("localhost","gohealth_test","iaminlove","gohealth_test");
// Check connection
if (mysqli_connect_errno())
  {
  echo "Failed to connect to MySQL: " . mysqli_connect_error();
  }

$result = mysqli_query($con,"SELECT * FROM recipe WHERE `recipeType` LIKE 'Seafood' ORDER BY RAND() LIMIT 1");

while($row = mysqli_fetch_array($result))
  {
  echo $row['recipeName'] /*. " " . $row['recipeType']*/;
  echo "<br>";
  }
?>


                            </div>

                            <div id="Chicken" name="Pro4" style="display:none">
                              <?php
$con=mysqli_connect("localhost","gohealth_test","iaminlove","gohealth_test");
// Check connection
if (mysqli_connect_errno())
  {
  echo "Failed to connect to MySQL: " . mysqli_connect_error();
  }

$result = mysqli_query($con,"SELECT * FROM recipe WHERE `recipeType` LIKE 'Chicken' ORDER BY RAND() LIMIT 1");

while($row = mysqli_fetch_array($result))
  {
  echo $row['recipeName'] /*. " " . $row['recipeType']*/;
  echo "<br>";
  }
?>



                                    </div>

</td>
                  </tr>
                  <tr>
                    <td>
                        <div id="Vegetable" name="Veg" style="display:none">

                          <?php
$con=mysqli_connect("localhost","gohealth_test","iaminlove","gohealth_test");
// Check connection
if (mysqli_connect_errno())
  {
  echo "Failed to connect to MySQL: " . mysqli_connect_error();
  }

$result = mysqli_query($con,"SELECT * FROM recipe WHERE `recipeType` LIKE 'Veggie' ORDER BY RAND() LIMIT 1");

while($row = mysqli_fetch_array($result))
  {
  echo $row['recipeName'] /*. " " . $row['recipeType']*/;
  echo "<br>";
  }
?>

                        </div>
                    </td>
                    <td><div id="Vegetable" name="Veg" style="display:none">

                          <?php
$con=mysqli_connect("localhost","gohealth_test","iaminlove","gohealth_test");
// Check connection
if (mysqli_connect_errno())
  {
  echo "Failed to connect to MySQL: " . mysqli_connect_error();
  }

$result = mysqli_query($con,"SELECT * FROM recipe WHERE `recipeType` LIKE 'Veggie' ORDER BY RAND() LIMIT 1");

while($row = mysqli_fetch_array($result))
  {
  echo $row['recipeName'] /*. " " . $row['recipeType']*/;
  echo "<br>";
  }
?>

                        </div>
                      </td>
                    <td><div id="Vegetable" name="Veg" style="display:none">

                          <?php
$con=mysqli_connect("localhost","gohealth_test","iaminlove","gohealth_test");
// Check connection
if (mysqli_connect_errno())
  {
  echo "Failed to connect to MySQL: " . mysqli_connect_error();
  }

$result = mysqli_query($con,"SELECT * FROM recipe WHERE `recipeType` LIKE 'Veggie' ORDER BY RAND() LIMIT 1");

while($row = mysqli_fetch_array($result))
  {
  echo $row['recipeName'] /*. " " . $row['recipeType']*/;
  echo "<br>";
  }
?>

                        </div>
                      </td>
                    <td><div id="Vegetable" name="Veg" style="display:none">

                          <?php
$con=mysqli_connect("localhost","gohealth_test","iaminlove","gohealth_test");
// Check connection
if (mysqli_connect_errno())
  {
  echo "Failed to connect to MySQL: " . mysqli_connect_error();
  }

$result = mysqli_query($con,"SELECT * FROM recipe WHERE `recipeType` LIKE 'Veggie' ORDER BY RAND() LIMIT 1");

while($row = mysqli_fetch_array($result))
  {
  echo $row['recipeName'] /*. " " . $row['recipeType']*/;
  echo "<br>";
  }
?>

                        </div>
                      </td>
                    <td><div id="Vegetable" name="Veg" style="display:none">

                          <?php
$con=mysqli_connect("localhost","gohealth_test","iaminlove","gohealth_test");
// Check connection
if (mysqli_connect_errno())
  {
  echo "Failed to connect to MySQL: " . mysqli_connect_error();
  }

$result = mysqli_query($con,"SELECT * FROM recipe WHERE `recipeType` LIKE 'Veggie' ORDER BY RAND() LIMIT 1");

while($row = mysqli_fetch_array($result))
  {
  echo $row['recipeName'] /*. " " . $row['recipeType']*/;
  echo "<br>";
  }
?>

                        </div></td>
                  </tr>
                  <tr>
                    <td><div id="Fruit" name="fruit" style="display:none">
                      <?php
$con=mysqli_connect("localhost","gohealth_test","iaminlove","gohealth_test");
// Check connection
if (mysqli_connect_errno())
  {
  echo "Failed to connect to MySQL: " . mysqli_connect_error();
  }

$result = mysqli_query($con,"SELECT * FROM recipe WHERE `recipeType` LIKE 'Fruit' ORDER BY RAND() LIMIT 1");

while($row = mysqli_fetch_array($result))
  {
  echo $row['recipeName'] /*. " " . $row['recipeType']*/;
  echo "<br>";
  }
?>

                        </div>


                    </td>
                    <td>
                      <div id="Fruit" name="fruit" style="display:none">
                        <?php
$con=mysqli_connect("localhost","gohealth_test","iaminlove","gohealth_test");
// Check connection
if (mysqli_connect_errno())
  {
  echo "Failed to connect to MySQL: " . mysqli_connect_error();
  }

$result = mysqli_query($con,"SELECT * FROM recipe WHERE `recipeType` LIKE 'Fruit' ORDER BY RAND() LIMIT 1");

while($row = mysqli_fetch_array($result))
  {
  echo $row['recipeName'] /*. " " . $row['recipeType']*/;
  echo "<br>";
  }
?>

                        </div>
                    </td>
                    <td>
                      <div id="Fruit" name="fruit" style="display:none">
                        <?php
$con=mysqli_connect("localhost","gohealth_test","iaminlove","gohealth_test");
// Check connection
if (mysqli_connect_errno())
  {
  echo "Failed to connect to MySQL: " . mysqli_connect_error();
  }

$result = mysqli_query($con,"SELECT * FROM recipe WHERE `recipeType` LIKE 'Fruit' ORDER BY RAND() LIMIT 1");

while($row = mysqli_fetch_array($result))
  {
  echo $row['recipeName'] /*. " " . $row['recipeType']*/;
  echo "<br>";
  }
?>

                        </div>
                    </td>
                    <td>
                      <div id="Fruit" name="fruit" style="display:none">

                        <?php
$con=mysqli_connect("localhost","gohealth_test","iaminlove","gohealth_test");
// Check connection
if (mysqli_connect_errno())
  {
  echo "Failed to connect to MySQL: " . mysqli_connect_error();
  }

$result = mysqli_query($con,"SELECT * FROM recipe WHERE `recipeType` LIKE 'Fruit' ORDER BY RAND() LIMIT 1");

while($row = mysqli_fetch_array($result))
  {
  echo $row['recipeName'] /*. " " . $row['recipeType']*/;
  echo "<br>";
  }
?>
                        </div>
                    </td>
                    <td>
                      <div id="Fruit" name="fruit" style="display:none">
                        <?php
$con=mysqli_connect("localhost","gohealth_test","iaminlove","gohealth_test");
// Check connection
if (mysqli_connect_errno())
  {
  echo "Failed to connect to MySQL: " . mysqli_connect_error();
  }

$result = mysqli_query($con,"SELECT * FROM recipe WHERE `recipeType` LIKE 'Fruit' ORDER BY RAND() LIMIT 2");

while($row = mysqli_fetch_array($result))
  {
  echo $row['recipeName'] /*. " " . $row['recipeType']*/;
  echo "<br>";
  
  }
?>

                        </div>
                    </td>
                  </tr>
                  <tr>
                    <td>Workout Mon</td>
                    <td>Workout Tues</td>
                    <td>Workout Wed</td>
                    <td>Workout Thurs</td>
                    <td>Workout Fri</td>
                  </tr>
                 </table> </center>     
                
                </div>




          </div>
      </div>
 </div>
                      </div>
                    </center>
      </div>
    </div>

    <div class="container">
      <!-- Example row of columns -->
      <div class="row">
        <div class="col-lg-4">
          <h2>Create.</h2>
          <p>It's as easy as clicking a button, literally. Start above to go through a small three step plan to becoming healthy. </p>
          <!-- <p><a class="btn btn-default" href="#">View details &raquo;</a></p> -->
        </div>
        <div class="col-lg-4">
          <h2>Save.</h2>
          <p>Save your plan then come back after you've finished. We're mobile accessible, so it shouldn't be a problem making another plan after your first week. Our log in feature will be coming soon. Stay Tuned! </p>
          <!-- <p><a class="btn btn-default" href="#">View details &raquo;</a></p> -->
       </div>
        <div class="col-lg-4">
          <h2>Workout.</h2>
          <p>Not only will you have a healthy diet, we've thrown in some workouts as well. This should speed up your goal to perfection.</p>
          <!-- <p><a class="btn btn-default" href="#">View details &raquo;</a></p> -->
        </div>
      </div>

      <hr>

      <footer>
        <p>&copy; Health Builder 2013</p>
      </footer>
    </div> <!-- /container -->


    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="dist/js/slider-form.js"></script>
    <script src="assets/js/jquery.js"></script>
    <script src="dist/js/bootstrap.min.js"></script>
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.1/jquery.min.js"></script> 
<script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.8.9/jquery-ui.min.js" type="text/javascript"></script> 
<script src="/bootstrap/js/bootstrap.min.js"></script> 
<script src="/js/slider_input.js"></script>
<script src="/testtyler2.php"></script>

<link rel="stylesheet" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.9/themes/base/jquery-ui.css" type="text/css" media="all" />
<link href="/bootstrap/css/bootstrap.min.css" rel="stylesheet" media="screen">

  </body>
</html>
