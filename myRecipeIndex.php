<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="food, points, health">
    <meta name="author" content="Christina Bijayananda & Alexander Okoli">
    <link rel="shortcut icon" href="images/favicon.ico">
    
     <!-- The 'html' CSS style is to remove shakyness of screen (when loaded) caused by the scrollbar!-->
	<style> 
		html { overflow-y: scroll; } 
	</style>

    <title>HealthBuilder Recipes</title>

    <!-- Bootstrap core CSS -->
    <link href="dist/css/bootstrap.css" rel="stylesheet">
    <!-- Custom styles for this template -->
    <link href="jumbotron.css" rel="stylesheet">
	<script src="http://code.jquery.com/jquery-1.9.1.js"></script>
	<script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
	
	<!--fancybox-->
	<link rel="stylesheet" href="fancybox/source/jquery.fancybox.css?v=2.1.5" type="text/css" media="screen" />
	<script type="text/javascript" src="fancybox/source/jquery.fancybox.pack.js?v=2.1.5"></script>
	
	<script type="text/javascript"> 
  
	 jQuery(document).ready(function($){
					
					//added for the dynamic display of images
					var addImage = function(link) {
									//$("#img-container").empty().append('<a id="single_2" class="fancybox-thumbs" data-fancybox-group="thumb" href="#">' +
										//	'<img src="' + link + '" alt="" />' +
											//'</a>');
											
									$("#img-container").empty().append('<a id="single_2" class="fancybox-thumbs" data-fancybox-group="thumb" href="' + link + '">' +
											'<img src="' + link + '" alt="" />' +
											'</a>');
											
									$("#single_2").fancybox({
													openEffect	: 'elastic',
													closeEffect	: 'elastic',

													helpers : {
														title : {
															type : 'inside'
														}
													}
												});
												
									console.log($("#single_2").length);
									$("#single_2").trigger( "click" );
					};
					
					

					$('#recipe').autocomplete({
							source:'suggest_recipe.php', 
							minLength:2,
				
						
						
						focus: function(event, ui) {
									$("#recipe").val(ui.item.label);
												return false;
											},
						select: function(event, ui) {
									var $selectedObj = ui.item;
									addImage("images/" + $selectedObj.link);

									return false;
								}
						}).data("ui-autocomplete")._renderItem = function(ul, item) {
							return $("<li>")
									.append("<a>" + item.label + "</a>")
									.appendTo(ul);
							};
								

						
					
				$('.fancybox').fancybox();	
			
	});
	
	
	
	function __highlight(s, t) {
	  var matcher = new RegExp("("+$.ui.autocomplete.escapeRegex(t)+")", "ig" );
	  return s.replace(matcher, "<strong>$1</strong>");
	}

  </script>
	
  </head>

  <style>
  #tab-container {
    overflow:hidden;
}
</style>

  <body>
  <!-- test code -->
  <img src="" id="image-holder"/>
	<!-- NavBar on top // Recipe Search Nav added here! Remove when test is complete-->
    <div class="navbar navbar-inverse navbar-fixed-top">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="http://gohealthbuilder.com/">Health Builder</a>
        </div>
        <div class="navbar-collapse collapse">
          <ul class="nav navbar-nav">
            <li class="active"><a href="http://gohealthbuilder.com/">Home</a></li>
            <li><a href="http://gohealthbuilder.com/bootstrap-3.0.0/examples/starter-template/about.html">About</a></li>
            <li><a href="http://gohealthbuilder.com/bootstrap-3.0.0/examples/starter-template/contact.html">Contact</a></li>
            <li><a href="Recipe Search/RecipeSearch.html">Recipe Search</a></li>
             
          </ul>
          <form class="navbar-form navbar-right">
            <div class="form-group">
              <input type="text" placeholder="Email" class="form-control">
            </div>
            <div class="form-group">
              <input type="password" placeholder="Password" class="form-control">
            </div>
            <button type="submit" class="btn btn-success">Sign in</button>
          </form>
        </div><!--/.navbar-collapse -->
      </div>
    </div>

	<div class="jumbotron">
      <div class="container">
        <center><h1>Health Builder</h1></center>
        <center><p>Don't bother handling you're health, we'll do it for you!</p></center>
          
              <div class="accordion" id="monogram-acc">
                  <div class="accordion-group">
                      <div class="accordion-heading">
                          <center><div class="btn btn-primary btn-lg" data-toggle="collapse" data-parent="#monogram-acc" href="#Animals">BUILD YOUR SCHEDULE</div></center>
						  <div>  </div>
						  <center><div class="btn btn-primary btn-lg"><a href="myPointsIndex.php">POINTS SYSTEM </a></div></center>
                      </div>
                      <center><div class="accordion-body collapse" id="Animals"><br>
                      <div class="container">
    <div class="container">
  <div class="row">
    <div class="span12">

     <div class="panel-group" id="accordion">
		<div class="panel panel-default">
			<div class="panel-heading">
		  <h4 class="panel-title">
			<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
			  Please find recipes here!
			</a>
		  </h4>
		</div>
				<div id="collapseOne" class="panel-collapse collapse in">
		<div class="panel-body">
			<div class="btn-group" data-toggle="buttons">
					AutoComplete will help you choose from the menu
					
					<!--<a id="single_2" href="http://farm4.staticflickr.com/3818/9036037912_83576fe5ab_b.jpg" title="forest mood (halina-anna)"></a> -->
			<!--<img src="http://farm4.staticflickr.com/3818/9036037912_83576fe5ab_m.jpg" alt="" />-->
		
			</div>
			</div>
		</div>
		
	</div>
	<div class="panel panel-default">
			<div class="panel-heading">
			  <h4 class="panel-title">
				<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">
				  Let's Find a Recipe in the database
				</a>
			  </h4>
			</div>
			<div id="collapseTwo" class="panel-collapse collapse">
			  
			  <div class="panel-body">
					<form action="suggest_recipe.php" method="post">
						<div class="form-group">
						  <input type="text" id="recipe" placeholder="Find a Recipe" class="form-control">
						</div>
						<div id="img-container" style="display: none;">
						</div>
					</form>
						
				  </div>
			  </div>
			</div>
	
		
		
 <div class="panel panel-default">
	<div class="panel-heading">
      <h4 class="panel-title">
        <p>Would you like check your available points for the day and plan accordingly?</p>
			<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseSeven">
			  Click ME!!
			</a>
      </h4>
	  
    </div>
	<div id="collapseSeven" class="panel-collapse collapse">
		<div class="panel-body">
          <div class="btn-group" data-toggle="buttons">                
                <div class="col-lg-4">
				<script type="text/javascript">
					// Popup window code
					function newPopup(url) {
						popupWindow = window.open(
							url,'popUpWindow','height=700,width=800,left=10,top=10,resizable=yes,scrollbars=yes,toolbar=yes,menubar=no,location=no,directories=no,status=yes')
					}
					</script>
					<a href="JavaScript:newPopup('http://www.quackit.com/html/html_help.cfm');">Open a popup window</a>
				
				</div>
			</div>
		</div>
	</div>
	</div>
	</center>
      </div>
    </div>

	
	<div class="container">
      <!-- Example row of columns -->
      <div class="row">
        <div class="col-lg-4">
          <h2>Create.</h2>
          <p>It's as easy as clicking a button, literally. Start above to go through a small three step plan to becoming healthy. </p>
          <!-- <p><a class="btn btn-default" href="#">View details &raquo;</a></p> -->
        </div>
        <div class="col-lg-4">
          <h2>Save.</h2>
          <p>Save your plan then come back after you've finished. We're mobile accessible, so it shouldn't be a problem making another plan after your first week. Our log in feature will be coming soon. Stay Tuned! </p>
          <!-- <p><a class="btn btn-default" href="#">View details &raquo;</a></p> -->
       </div>
        <div class="col-lg-4">
          <h2>Workout.</h2>
          <p>Not only will you have a healthy diet, we've thrown in some workouts as well. This should speed up your goal to perfection.</p>
          <!-- <p><a class="btn btn-default" href="#">View details &raquo;</a></p> -->
        </div>
      </div>

      <hr>

      <footer>
        <p>&copy; Health Builder 2013</p>
      </footer>
    </div> <!-- /container -->

						<center><div class="accordion-body collapse" id="myAnimals"><br>
					

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="dist/js/slider-form.js"></script>
    <script src="assets/js/jquery.js"></script>
    <script src="dist/js/bootstrap.min.js"></script>
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.1/jquery.min.js"></script> 
<script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.8.9/jquery-ui.min.js" type="text/javascript"></script> 
<script src="/bootstrap/js/bootstrap.min.js"></script> 
<script src="/js/slider_input.js"></script>
<script src="/testtyler2.php"></script>

<link rel="stylesheet" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.9/themes/base/jquery-ui.css" type="text/css" media="all" />
<link href="/bootstrap/css/bootstrap.min.css" rel="stylesheet" media="screen">

  </body>

					
</html>
