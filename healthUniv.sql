-- phpMyAdmin SQL Dump
-- version 4.0.4
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Dec 06, 2013 at 03:46 AM
-- Server version: 5.6.12-log
-- PHP Version: 5.4.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `health`
--
CREATE DATABASE IF NOT EXISTS `health` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `health`;

-- --------------------------------------------------------

--
-- Table structure for table `fooditems`
--

CREATE TABLE IF NOT EXISTS `fooditems` (
  `ItemID` int(11) NOT NULL AUTO_INCREMENT,
  `SubItemTypeID` int(10) NOT NULL,
  `ItemName` varchar(50) NOT NULL,
  `ItemPoints` int(11) DEFAULT NULL,
  PRIMARY KEY (`ItemID`),
  KEY `SubItemTypeID` (`SubItemTypeID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=9 ;

--
-- Dumping data for table `fooditems`
--

INSERT INTO `fooditems` (`ItemID`, `SubItemTypeID`, `ItemName`, `ItemPoints`) VALUES
(1, 2, 'Grilled Thai Chicken Sandwich', 410),
(2, 2, 'Ham and Cheese Chicken', 236),
(3, 1, 'French Beef Stew', 297),
(5, 1, 'Asian Beef & Veggie Skewers', 228),
(8, 1, 'Braised Beef & Mushrooms', 269);

-- --------------------------------------------------------

--
-- Table structure for table `itemtype`
--

CREATE TABLE IF NOT EXISTS `itemtype` (
  `ItemTypeID` int(10) NOT NULL AUTO_INCREMENT,
  `ItemTypeName` varchar(50) NOT NULL,
  PRIMARY KEY (`ItemTypeID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `itemtype`
--

INSERT INTO `itemtype` (`ItemTypeID`, `ItemTypeName`) VALUES
(1, '--'),
(2, 'Protein'),
(3, 'Dessert'),
(4, 'Fruits'),
(5, 'Veggies');

-- --------------------------------------------------------

--
-- Table structure for table `person`
--

CREATE TABLE IF NOT EXISTS `person` (
  `PersonID` int(11) NOT NULL AUTO_INCREMENT,
  `FirstName` varchar(20) NOT NULL,
  `LastName` varchar(20) NOT NULL,
  `Email` varchar(50) NOT NULL,
  `Message` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`PersonID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `recipe`
--

CREATE TABLE IF NOT EXISTS `recipe` (
  `RecipeID` int(11) NOT NULL AUTO_INCREMENT,
  `RecipeName` varchar(100) NOT NULL,
  `RecipeType` varchar(50) NOT NULL,
  `Description` varchar(255) DEFAULT NULL,
  `RecipePoints` int(11) DEFAULT NULL,
  `imageLink` varchar(120) DEFAULT NULL,
  PRIMARY KEY (`RecipeID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `recipe`
--

INSERT INTO `recipe` (`RecipeID`, `RecipeName`, `RecipeType`, `Description`, `RecipePoints`, `imageLink`) VALUES
(1, 'Grilled Chicken Salad', 'Chicken', 'Grilled Chicken with vegetables ', 6, 'Jellyfish.jpg'),
(3, 'French Omlette', 'Egg', 'Egg Omlette with Sausage and Cheese', 6, NULL),
(4, 'Steamed Broccoli', 'Veggie', 'Steamed Broccoli with a hint of butter and seasoning', 1, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `recipeitems`
--

CREATE TABLE IF NOT EXISTS `recipeitems` (
  `RecipeItemsID` int(11) NOT NULL AUTO_INCREMENT,
  `RecipeID` int(11) NOT NULL,
  `ItemID` int(11) NOT NULL,
  PRIMARY KEY (`RecipeItemsID`),
  KEY `RecipeID` (`RecipeID`),
  KEY `ItemID` (`ItemID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `subitemtype`
--

CREATE TABLE IF NOT EXISTS `subitemtype` (
  `SubItemTypeID` int(10) NOT NULL AUTO_INCREMENT,
  `ItemTypeID` int(10) NOT NULL,
  `SubItemTypeName` varchar(50) NOT NULL,
  PRIMARY KEY (`SubItemTypeID`),
  KEY `ItemTypeID` (`ItemTypeID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=15 ;

--
-- Dumping data for table `subitemtype`
--

INSERT INTO `subitemtype` (`SubItemTypeID`, `ItemTypeID`, `SubItemTypeName`) VALUES
(1, 2, 'Beef'),
(2, 2, 'Chicken'),
(3, 2, 'Lamb'),
(4, 2, 'SeaFood'),
(5, 5, 'Broccoli'),
(6, 5, 'Asparagus'),
(7, 5, 'Spinach'),
(8, 5, 'Avacado'),
(9, 4, 'Apple'),
(10, 4, 'Oranges'),
(11, 3, 'Apple Pie'),
(12, 3, 'Strawberry Shortcake'),
(13, 2, 'Beans');

-- --------------------------------------------------------

--
-- Table structure for table `workout`
--

CREATE TABLE IF NOT EXISTS `workout` (
  `WorkOutID` int(11) NOT NULL AUTO_INCREMENT,
  `WorkOutName` varchar(100) NOT NULL,
  `WorkOutType` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`WorkOutID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `zipcode`
--

CREATE TABLE IF NOT EXISTS `zipcode` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `zip` varchar(20) NOT NULL,
  `city` varchar(100) NOT NULL,
  `state` varchar(100) NOT NULL,
  `county` varchar(75) NOT NULL,
  `fips` int(11) NOT NULL,
  `areacode` varchar(3) NOT NULL,
  `dst` enum('Y','N') NOT NULL,
  `timezone` varchar(20) NOT NULL,
  `lat` varchar(25) NOT NULL,
  `lon` varchar(25) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_zipcode` (`zip`(5))
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=204 ;

--
-- Dumping data for table `zipcode`
--

INSERT INTO `zipcode` (`id`, `zip`, `city`, `state`, `county`, `fips`, `areacode`, `dst`, `timezone`, `lat`, `lon`) VALUES
(1, '00501', 'Holtsville', 'NY', 'Suffolk', 36103, '631', 'Y', 'EST', '40.8151', '73.0455'),
(2, '00544', 'Holtsville', 'NY', 'Suffolk', 36103, '631', 'Y', 'EST', '40.8132', '73.0476'),
(3, '00601', 'Adjuntas', 'PR', 'Adjuntas', 72001, '787', 'N', 'EST+1', '18.1642', '66.7227'),
(4, '00602', 'Aguada', 'PR', 'Aguada', 72003, '787', 'N', 'EST+1', '18.3974', '67.1679'),
(5, '00603', 'Aguadilla', 'PR', 'Aguadilla', 72005, '787', 'N', 'EST+1', '18.4409', '67.1508'),
(6, '00604', 'Aguadilla', 'PR', 'Aguadilla', 72005, '787', 'N', 'EST+1', '18.4295', '67.1547'),
(7, '00605', 'Aguadilla', 'PR', 'Aguadilla', 72005, '787', 'N', 'EST+1', '18.4289', '67.1538'),
(8, '00606', 'Maricao', 'PR', 'Maricao', 72093, '787', 'N', 'EST+1', '18.1825', '66.9805'),
(9, '00610', 'Anasco', 'PR', 'Anasco', 72011, '787', 'N', 'EST+1', '18.2859', '67.1412'),
(10, '00611', 'Angeles', 'PR', 'Utuado', 72141, '787', 'N', 'EST+1', '18.2856', '66.9698'),
(11, '00612', 'Arecibo', 'PR', 'Arecibo', 72013, '787', 'N', 'EST+1', '18.4739', '66.7295'),
(12, '00613', 'Arecibo', 'PR', 'Arecibo', 72013, '787', 'N', 'EST+1', '18.4748', '66.7215'),
(13, '00614', 'Arecibo', 'PR', 'Arecibo', 72013, '787', 'N', 'EST+1', '18.4567', '66.7358'),
(14, '00616', 'Bajadero', 'PR', 'Arecibo', 72013, '787', 'N', 'EST+1', '18.4287', '66.6836'),
(15, '00617', 'Barceloneta', 'PR', 'Barceloneta', 72017, '787', 'N', 'EST+1', '18.4548', '66.5389'),
(16, '00622', 'Boqueron', 'PR', 'Cabo Rojo', 72023, '787', 'N', 'EST+1', '18.0265', '67.1727'),
(17, '00623', 'Cabo Rojo', 'PR', 'Cabo Rojo', 72023, '787', 'N', 'EST+1', '18.0849', '67.1633'),
(18, '00624', 'Penuelas', 'PR', 'Penuelas', 72111, '787', 'N', 'EST+1', '18.0112', '66.3514'),
(19, '00627', 'Camuy', 'PR', 'Camuy', 72027, '787', 'N', 'EST+1', '18.4865', '66.8455'),
(20, '00631', 'Castaner', 'PR', 'Lares', 72081, '787', 'N', 'EST+1', '18.1639', '66.7245'),
(21, '00636', 'Rosario', 'PR', 'San German', 72125, '787', 'N', 'EST+1', '18.1647', '67.0797'),
(22, '00637', 'Sabana Grande', 'PR', 'Sabana Grande', 72121, '787', 'N', 'EST+1', '18.0664', '66.9568'),
(23, '00638', 'Ciales', 'PR', 'Ciales', 72039, '787', 'N', 'EST+1', '18.3392', '66.4685'),
(24, '00641', 'Utuado', 'PR', 'Utuado', 72141, '787', 'N', 'EST+1', '18.2676', '66.7005'),
(25, '00646', 'Dorado', 'PR', 'Dorado', 72051, '787', 'N', 'EST+1', '18.4598', '66.2592'),
(26, '00647', 'Ensenada', 'PR', 'Guanica', 72055, '787', 'N', 'EST+1', '17.9715', '66.9368'),
(27, '00650', 'Florida', 'PR', 'Florida', 72054, '787', 'N', 'EST+1', '18.3645', '66.5673'),
(28, '00652', 'Garrochales', 'PR', 'Arecibo', 72013, '787', 'N', 'EST+1', '18.4598', '66.6117'),
(29, '00653', 'Guanica', 'PR', 'Guanica', 72055, '787', 'N', 'EST+1', '17.9714', '66.9064'),
(30, '00656', 'Guayanilla', 'PR', 'Guayanilla', 72059, '787', 'N', 'EST+1', '18.0206', '66.7913'),
(31, '00659', 'Hatillo', 'PR', 'Hatillo', 72065, '787', 'N', 'EST+1', '18.4851', '66.8257'),
(32, '00660', 'Hormigueros', 'PR', 'Hormigueros', 72067, '787', 'N', 'EST+1', '18.1684', '67.1306'),
(33, '00662', 'Isabela', 'PR', 'Isabela', 72071, '787', 'N', 'EST+1', '18.4951', '67.0273'),
(34, '00664', 'Jayuya', 'PR', 'Jayuya', 72073, '787', 'N', 'EST+1', '18.2205', '66.6046'),
(35, '00667', 'Lajas', 'PR', 'Lajas', 72079, '787', 'N', 'EST+1', '18.0409', '67.0495'),
(36, '00669', 'Lares', 'PR', 'Lares', 72081, '787', 'N', 'EST+1', '18.2912', '66.8528'),
(37, '00670', 'Las Marias', 'PR', 'Las Marias', 72083, '787', 'N', 'EST+1', '18.2539', '66.9876'),
(38, '00674', 'Manati', 'PR', 'Manati', 72091, '787', 'N', 'EST+1', '18.4259', '66.4717'),
(39, '00676', 'Moca', 'PR', 'Moca', 72099, '787', 'N', 'EST+1', '18.3946', '67.1158'),
(40, '00677', 'Rincon', 'PR', 'Rincon', 72117, '787', 'N', 'EST+1', '18.3427', '67.2504'),
(41, '00678', 'Quebradillas', 'PR', 'Quebradillas', 72115, '787', 'N', 'EST+1', '18.4738', '66.9364'),
(42, '00680', 'Mayaguez', 'PR', 'Mayaguez', 72097, '787', 'N', 'EST+1', '18.2038', '67.1453'),
(43, '00681', 'Mayaguez', 'PR', 'Mayaguez', 72097, '787', 'N', 'EST+1', '18.2035', '67.1433'),
(44, '00682', 'Mayaguez', 'PR', 'Mayaguez', 72097, '787', 'N', 'EST+1', '18.2030', '67.1400'),
(45, '00683', 'San German', 'PR', 'San German', 72125, '787', 'N', 'EST+1', '18.0846', '67.0463'),
(46, '00685', 'San Sebastian', 'PR', 'San Sebastian', 72131, '787', 'N', 'EST+1', '18.3413', '66.9910'),
(47, '00687', 'Morovis', 'PR', 'Morovis', 72101, '787', 'N', 'EST+1', '18.3346', '66.4186'),
(48, '00688', 'Sabana Hoyos', 'PR', 'Arecibo', 72013, '787', 'N', 'EST+1', '18.4356', '66.6145'),
(49, '00690', 'San Antonio', 'PR', 'Aguadilla', 72005, '787', 'N', 'EST+1', '18.4943', '67.0987'),
(50, '00692', 'Vega Alta', 'PR', 'Vega Alta', 72143, '787', 'N', 'EST+1', '18.4144', '66.3319'),
(51, '00693', 'Vega Baja', 'PR', 'Vega Baja', 72145, '787', 'N', 'EST+1', '18.4061', '66.3012'),
(52, '00694', 'Vega Baja', 'PR', 'Vega Baja', 72145, '787', 'N', 'EST+1', '18.4465', '66.3884'),
(53, '00698', 'Yauco', 'PR', 'Yauco', 72153, '787', 'N', 'EST+1', '18.0306', '66.8507'),
(54, '00703', 'Aguas Buenas', 'PR', 'Aguas Buenas', 72007, '787', 'N', 'EST+1', '18.2584', '66.1068'),
(55, '00704', 'Aguirre', 'PR', 'Guayama', 72057, '787', 'N', 'EST+1', '18.2501', '66.1032'),
(56, '00705', 'Aibonito', 'PR', 'Aibonito', 72009, '787', 'N', 'EST+1', '18.1439', '66.2725'),
(57, '00707', 'Maunabo', 'PR', 'Maunabo', 72095, '787', 'N', 'EST+1', '18.0098', '65.8982'),
(58, '00714', 'Arroyo', 'PR', 'Arroyo', 72015, '787', 'N', 'EST+1', '17.9678', '66.0617'),
(59, '00715', 'Mercedita', 'PR', 'Ponce', 72113, '787', 'N', 'EST+1', '18.1144', '66.8568'),
(60, '00716', 'Ponce', 'PR', 'Ponce', 72113, '787', 'N', 'EST+1', '18.0093', '66.6209'),
(61, '00717', 'Ponce', 'PR', 'Ponce', 72113, '787', 'N', 'EST+1', '18.0055', '66.6209'),
(62, '00718', 'Naguabo', 'PR', 'Naguabo', 72103, '787', 'N', 'EST+1', '18.2147', '65.7358'),
(63, '00719', 'Naranjito', 'PR', 'Naranjito', 72105, '787', 'N', 'EST+1', '18.3023', '66.2460'),
(64, '00720', 'Orocovis', 'PR', 'Orocovis', 72107, '787', 'N', 'EST+1', '18.2314', '66.3905'),
(65, '00721', 'Palmer', 'PR', 'Rio Grande', 72119, '787', 'N', 'EST+1', '18.3018', '66.0800'),
(66, '00723', 'Patillas', 'PR', 'Patillas', 72109, '787', 'N', 'EST+1', '18.0086', '66.0144'),
(67, '00725', 'Caguas', 'PR', 'Caguas', 72025, '787', 'N', 'EST+1', '18.2361', '66.0369'),
(68, '00726', 'Caguas', 'PR', 'Caguas', 72025, '787', 'N', 'EST+1', '18.2364', '66.0489'),
(69, '00727', 'Caguas', 'PR', 'Caguas', 72025, '787', 'N', 'EST+1', '18.2351', '66.0374'),
(70, '00728', 'Ponce', 'PR', 'Ponce', 72113, '787', 'N', 'EST+1', '18.0073', '66.5912'),
(71, '00729', 'Canovanas', 'PR', 'Canovanas', 72029, '787', 'N', 'EST+1', '18.3747', '65.9126'),
(72, '00730', 'Ponce', 'PR', 'Ponce', 72113, '787', 'N', 'EST+1', '18.0152', '66.6246'),
(73, '00731', 'Ponce', 'PR', 'Ponce', 72113, '787', 'N', 'EST+1', '18.0179', '66.6229'),
(74, '00732', 'Ponce', 'PR', 'Ponce', 72113, '787', 'N', 'EST+1', '18.0133', '66.6146'),
(75, '00733', 'Ponce', 'PR', 'Ponce', 72113, '787', 'N', 'EST+1', '18.0133', '66.6146'),
(76, '00734', 'Ponce', 'PR', 'Ponce', 72113, '787', 'N', 'EST+1', '18.0133', '66.6146'),
(77, '00735', 'Ceiba', 'PR', 'Ceiba', 72037, '787', 'N', 'EST+1', '18.2667', '65.6476'),
(78, '00736', 'Cayey', 'PR', 'Cayey', 72035, '787', 'N', 'EST+1', '18.1193', '66.1642'),
(79, '00737', 'Cayey', 'PR', 'Cayey', 72035, '787', 'N', 'EST+1', '18.1138', '66.1667'),
(80, '00738', 'Fajardo', 'PR', 'Fajardo', 72053, '787', 'N', 'EST+1', '18.3304', '65.6573'),
(81, '00739', 'Cidra', 'PR', 'Cidra', 72041, '787', 'N', 'EST+1', '18.1743', '66.1581'),
(82, '00740', 'Puerto Real', 'PR', 'Fajardo', 72053, '787', 'N', 'EST+1', '18.3201', '65.6502'),
(83, '00741', 'Punta Santiago', 'PR', 'Humacao', 72069, '787', 'N', 'EST+1', '18.1684', '65.7486'),
(84, '00742', 'Roosevelt Roads', 'PR', 'Ceiba', 72037, '787', 'N', 'EST+1', '18.2666', '65.6482'),
(85, '00744', 'Rio Blanco', 'PR', 'Naguabo', 72103, '787', 'N', 'EST+1', '18.2202', '65.7902'),
(86, '00745', 'Rio Grande', 'PR', 'Rio Grande', 72119, '787', 'N', 'EST+1', '18.3817', '65.8302'),
(87, '00751', 'Salinas', 'PR', 'Salinas', 72123, '787', 'N', 'EST+1', '17.9903', '66.2815'),
(88, '00754', 'San Lorenzo', 'PR', 'San Lorenzo', 72129, '787', 'N', 'EST+1', '18.1909', '65.9618'),
(89, '00757', 'Santa Isabel', 'PR', 'Santa Isabel', 72133, '787', 'N', 'EST+1', '17.9676', '66.4046'),
(90, '00765', 'Vieques', 'PR', 'Vieques', 72147, '787', 'N', 'EST+1', '18.1015', '65.4768'),
(91, '00766', 'Villalba', 'PR', 'Villalba', 72149, '787', 'N', 'EST+1', '18.1304', '66.4926'),
(92, '00767', 'Yabucoa', 'PR', 'Yabucoa', 72151, '787', 'N', 'EST+1', '18.0547', '65.8738'),
(93, '00769', 'Coamo', 'PR', 'Coamo', 72043, '787', 'N', 'EST+1', '18.0836', '66.3566'),
(94, '00771', 'Las Piedras', 'PR', 'Las Piedras', 72085, '787', 'N', 'EST+1', '18.1853', '65.8667'),
(95, '00772', 'Loiza', 'PR', 'Loiza', 72087, '787', 'N', 'EST+1', '18.2022', '65.8600'),
(96, '00773', 'Luquillo', 'PR', 'Luquillo', 72089, '787', 'N', 'EST+1', '18.3744', '65.7168'),
(97, '00775', 'Culebra', 'PR', 'Culebra', 72049, '787', 'N', 'EST+1', '18.3053', '65.3114'),
(98, '00777', 'Juncos', 'PR', 'Juncos', 72077, '787', 'N', 'EST+1', '18.2339', '65.9218'),
(99, '00778', 'Gurabo', 'PR', 'Gurabo', 72063, '787', 'N', 'EST+1', '18.2551', '65.9776'),
(100, '00780', 'Coto Laurel', 'PR', 'Ponce', 72113, '787', 'N', 'EST+1', '18.0474', '66.5517'),
(101, '00782', 'Comerio', 'PR', 'Comerio', 72045, '787', 'N', 'EST+1', '18.2202', '66.2266'),
(102, '00783', 'Corozal', 'PR', 'Corozal', 72047, '787', 'N', 'EST+1', '18.3429', '66.3176'),
(103, '00784', 'Guayama', 'PR', 'Guayama', 72057, '787', 'N', 'EST+1', '17.9849', '66.1175'),
(104, '00785', 'Guayama', 'PR', 'Guayanilla', 72059, '787', 'N', 'EST+1', '17.9800', '66.1101'),
(105, '00786', 'La Plata', 'PR', 'Aibonito', 72009, '787', 'N', 'EST+1', '18.1564', '66.2336'),
(106, '00791', 'Humacao', 'PR', 'Humacao', 72069, '787', 'N', 'EST+1', '18.1494', '65.8294'),
(107, '00792', 'Humacao', 'PR', 'Humacao', 72069, '787', 'N', 'EST+1', '18.1519', '65.8279'),
(108, '00794', 'Barranquitas', 'PR', 'Barranquitas', 72019, '787', 'N', 'EST+1', '18.1808', '66.2956'),
(109, '00795', 'Juana Diaz', 'PR', 'Juana Diaz', 72075, '787', 'N', 'EST+1', '18.0561', '66.5063'),
(110, '00801', 'St Thomas', 'VI', 'Saint Thomas', 78030, '340', 'N', 'EST+1', '18.3436', '64.9314'),
(111, '00802', 'St Thomas', 'VI', 'Saint Thomas', 78030, '340', 'N', 'EST+1', '18.3436', '64.9322'),
(112, '00803', 'St Thomas', 'VI', 'Saint Thomas', 78030, '340', 'N', 'EST+1', '18.3436', '64.9314'),
(113, '00804', 'St Thomas', 'VI', 'Saint Thomas', 78030, '340', 'N', 'EST+1', '18.3436', '64.9314'),
(114, '00805', 'St Thomas', 'VI', 'Saint Thomas', 78030, '340', 'N', 'EST+1', '18.3436', '64.9314'),
(115, '00820', 'Christiansted', 'VI', 'Saint Croix', 78010, '340', 'N', 'EST+1', '17.7473', '64.7041'),
(116, '00821', 'Christiansted', 'VI', 'Saint Croix', 78010, '340', 'N', 'EST+1', '17.7488', '64.7039'),
(117, '00822', 'Christiansted', 'VI', 'Saint Croix', 78010, '340', 'N', 'EST+1', '17.7488', '64.7039'),
(118, '00823', 'Christiansted', 'VI', 'Saint Croix', 78010, '340', 'N', 'EST+1', '17.7488', '64.7039'),
(119, '00824', 'Christiansted', 'VI', 'Saint Croix', 78010, '340', 'N', 'EST+1', '17.7488', '64.7039'),
(120, '00830', 'St John', 'VI', 'Saint John', 78020, '340', 'N', 'EST+1', '18.3328', '64.7926'),
(121, '00831', 'St John', 'VI', 'Saint John', 78020, '340', 'N', 'EST+1', '18.3333', '64.7943'),
(122, '00840', 'Frederiksted', 'VI', 'Saint Croix', 78010, '340', 'N', 'EST+1', '17.7135', '64.8827'),
(123, '00841', 'Frederiksted', 'VI', 'Saint Croix', 78010, '340', 'N', 'EST+1', '17.7147', '64.8818'),
(124, '00850', 'Kingshill', 'VI', 'Saint Croix', 78010, '340', 'N', 'EST+1', '17.7193', '64.8336'),
(125, '00851', 'Kingshill', 'VI', 'Saint Croix', 78010, '340', 'N', 'EST+1', '17.7253', '64.7836'),
(126, '00901', 'San Juan', 'PR', 'San Juan', 72127, '787', 'N', 'EST+1', '18.4639', '66.1098'),
(127, '00902', 'San Juan', 'PR', 'San Juan', 72127, '787', 'N', 'EST+1', '18.4465', '66.0984'),
(128, '00906', 'San Juan', 'PR', 'San Juan', 72127, '787', 'N', 'EST+1', '18.4083', '66.0644'),
(129, '00907', 'San Juan', 'PR', 'San Juan', 72127, '787', 'N', 'EST+1', '18.4559', '66.0777'),
(130, '00908', 'San Juan', 'PR', 'San Juan', 72127, '787', 'N', 'EST+1', '18.4462', '66.0799'),
(131, '00909', 'San Juan', 'PR', 'San Juan', 72127, '787', 'N', 'EST+1', '18.4452', '66.0690'),
(132, '00910', 'San Juan', 'PR', 'San Juan', 72127, '787', 'N', 'EST+1', '18.4406', '66.0687'),
(133, '00911', 'San Juan', 'PR', 'San Juan', 72127, '787', 'N', 'EST+1', '18.4527', '66.0579'),
(134, '00912', 'San Juan', 'PR', 'San Juan', 72127, '787', 'N', 'EST+1', '18.4475', '66.0594'),
(135, '00913', 'San Juan', 'PR', 'San Juan', 72127, '787', 'N', 'EST+1', '18.4509', '66.0447'),
(136, '00914', 'San Juan', 'PR', 'San Juan', 72127, '787', 'N', 'EST+1', '18.4458', '66.0159'),
(137, '00915', 'San Juan', 'PR', 'San Juan', 72127, '787', 'N', 'EST+1', '18.4393', '66.0493'),
(138, '00916', 'San Juan', 'PR', 'San Juan', 72127, '787', 'N', 'EST+1', '18.3780', '66.1638'),
(139, '00917', 'San Juan', 'PR', 'San Juan', 72127, '787', 'N', 'EST+1', '18.4238', '66.0515'),
(140, '00918', 'San Juan', 'PR', 'San Juan', 72127, '787', 'N', 'EST+1', '18.4170', '66.0617'),
(141, '00919', 'San Juan', 'PR', 'San Juan', 72127, '787', 'N', 'EST+1', '18.4128', '66.0650'),
(142, '00920', 'San Juan', 'PR', 'San Juan', 72127, '787', 'N', 'EST+1', '18.4080', '66.0933'),
(143, '00921', 'San Juan', 'PR', 'San Juan', 72127, '787', 'N', 'EST+1', '18.3905', '66.0903'),
(144, '00922', 'San Juan', 'PR', 'San Juan', 72127, '787', 'N', 'EST+1', '18.4076', '66.0925'),
(145, '00923', 'San Juan', 'PR', 'San Juan', 72127, '787', 'N', 'EST+1', '18.4125', '66.0374'),
(146, '00924', 'San Juan', 'PR', 'San Juan', 72127, '787', 'N', 'EST+1', '18.4051', '66.0126'),
(147, '00925', 'San Juan', 'PR', 'San Juan', 72127, '787', 'N', 'EST+1', '18.4067', '66.0993'),
(148, '00926', 'San Juan', 'PR', 'San Juan', 72127, '787', 'N', 'EST+1', '18.3754', '66.0577'),
(149, '00927', 'San Juan', 'PR', 'San Juan', 72127, '787', 'N', 'EST+1', '18.3878', '66.0742'),
(150, '00928', 'San Juan', 'PR', 'San Juan', 72127, '787', 'N', 'EST+1', '18.3920', '66.0624'),
(151, '00929', 'San Juan', 'PR', 'San Juan', 72127, '787', 'N', 'EST+1', '18.4008', '66.0262'),
(152, '00930', 'San Juan', 'PR', 'San Juan', 72127, '787', 'N', 'EST+1', '18.4083', '66.0644'),
(153, '00931', 'San Juan', 'PR', 'San Juan', 72127, '787', 'N', 'EST+1', '18.4083', '66.0644'),
(154, '00933', 'San Juan', 'PR', 'San Juan', 72127, '787', 'N', 'EST+1', '18.4683', '66.1061'),
(155, '00934', 'Fort Buchanan', 'PR', 'Bayamon', 72021, '787', 'N', 'EST+1', '18.4601', '66.1102'),
(156, '00935', 'San Juan', 'PR', 'San Juan', 72127, '787', 'N', 'EST+1', '18.4083', '66.0644'),
(157, '00936', 'San Juan', 'PR', 'San Juan', 72127, '787', 'N', 'EST+1', '18.3829', '66.0647'),
(158, '00937', 'San Juan', 'PR', 'San Juan', 72127, '787', 'N', 'EST+1', '18.4083', '66.0644'),
(159, '00938', 'San Juan', 'PR', 'San Juan', 72127, '787', 'N', 'EST+1', '18.4106', '66.0629'),
(160, '00939', 'San Juan', 'PR', 'San Juan', 72127, '787', 'N', 'EST+1', '18.4083', '66.0644'),
(161, '00940', 'San Juan', 'PR', 'San Juan', 72127, '787', 'N', 'EST+1', '18.4108', '66.0650'),
(162, '00949', 'Toa Baja', 'PR', 'Toa Baja', 72137, '787', 'N', 'EST+1', '18.4519', '66.1817'),
(163, '00950', 'Toa Baja', 'PR', 'Toa Baja', 72137, '787', 'N', 'EST+1', '18.4456', '66.2600'),
(164, '00951', 'Toa Baja', 'PR', 'Toa Baja', 72137, '787', 'N', 'EST+1', '18.4456', '66.2600'),
(165, '00952', 'Sabana Seca', 'PR', 'Toa Baja', 72137, '787', 'N', 'EST+1', '18.4257', '66.1885'),
(166, '00953', 'Toa Alta', 'PR', 'Toa Alta', 72135, '787', 'N', 'EST+1', '18.3893', '66.2481'),
(167, '00954', 'Toa Alta', 'PR', 'Toa Alta', 72135, '787', 'N', 'EST+1', '18.3900', '66.2501'),
(168, '00955', 'San Juan', 'PR', 'San Juan', 72127, '787', 'N', 'EST+1', '18.4083', '66.0644'),
(169, '00956', 'Bayamon', 'PR', 'Bayamon', 72021, '787', 'N', 'EST+1', '18.1892', '66.1134'),
(170, '00957', 'Bayamon', 'PR', 'Bayamon', 72021, '787', 'N', 'EST+1', '18.1783', '66.1111'),
(171, '00958', 'Bayamon', 'PR', 'Bayamon', 72021, '787', 'N', 'EST+1', '18.1797', '66.1135'),
(172, '00959', 'Bayamon', 'PR', 'Bayamon', 72021, '787', 'N', 'EST+1', '18.1781', '66.1167'),
(173, '00960', 'Bayamon', 'PR', 'Bayamon', 72021, '787', 'N', 'EST+1', '18.1797', '66.1135'),
(174, '00961', 'Bayamon', 'PR', 'Bayamon', 72021, '787', 'N', 'EST+1', '18.1808', '66.1122'),
(175, '00962', 'Catano', 'PR', 'Catano', 72033, '787', 'N', 'EST+1', '18.4443', '66.1386'),
(176, '00963', 'Catano', 'PR', 'Catano', 72033, '787', 'N', 'EST+1', '18.4462', '66.1402'),
(177, '00965', 'Guaynabo', 'PR', 'Guaynabo', 72061, '787', 'N', 'EST+1', '18.3658', '66.0928'),
(178, '00966', 'Guaynabo', 'PR', 'Guaynabo', 72061, '787', 'N', 'EST+1', '18.3882', '66.1144'),
(179, '00968', 'Guaynabo', 'PR', 'San Juan', 72127, '787', 'N', 'EST+1', '18.3864', '66.1143'),
(180, '00969', 'Guaynabo', 'PR', 'Guaynabo', 72061, '787', 'N', 'EST+1', '18.3866', '66.1135'),
(181, '00970', 'Guaynabo', 'PR', 'Guaynabo', 72061, '787', 'N', 'EST+1', '18.3864', '66.1143'),
(182, '00971', 'Guaynabo', 'PR', 'Guaynabo', 72061, '787', 'N', 'EST+1', '18.3885', '66.1150'),
(183, '00975', 'San Juan', 'PR', 'San Juan', 72127, '787', 'N', 'EST+1', '18.4083', '66.0644'),
(184, '00976', 'Trujillo Alto', 'PR', 'Trujillo Alto', 72139, '787', 'N', 'EST+1', '18.3569', '66.0247'),
(185, '00977', 'Trujillo Alto', 'PR', 'Trujillo Alto', 72139, '787', 'N', 'EST+1', '18.3569', '66.0076'),
(186, '00978', 'Saint Just', 'PR', 'Trujillo Alto', 72139, '787', 'N', 'EST+1', '18.3703', '66.0123'),
(187, '00979', 'Carolina', 'PR', 'San Juan', 72127, '787', 'N', 'EST+1', '18.4048', '65.9789'),
(188, '00981', 'Carolina', 'PR', 'San Juan', 72127, '787', 'N', 'EST+1', '18.4083', '65.9810'),
(189, '00982', 'Carolina', 'PR', 'San Juan', 72127, '787', 'N', 'EST+1', '18.4078', '65.9794'),
(190, '00983', 'Carolina', 'PR', 'Carolina', 72031, '787', 'N', 'EST+1', '18.4298', '65.9816'),
(191, '00984', 'Carolina', 'PR', 'Carolina', 72031, '787', 'N', 'EST+1', '18.4089', '65.9578'),
(192, '00985', 'Carolina', 'PR', 'Carolina', 72031, '787', 'N', 'EST+1', '18.3988', '65.9524'),
(193, '00986', 'Carolina', 'PR', 'Carolina', 72031, '787', 'N', 'EST+1', '18.4089', '65.9812'),
(194, '00987', 'Carolina', 'PR', 'Carolina', 72031, '787', 'N', 'EST+1', '18.4083', '65.9810'),
(195, '00988', 'Carolina', 'PR', 'Carolina', 72031, '787', 'N', 'EST+1', '18.4067', '65.9803'),
(196, '01001', 'Agawam', 'MA', 'Hampden', 25013, '413', 'Y', 'EST', '42.0626', '72.6252'),
(197, '01002', 'Amherst', 'MA', 'Hampshire', 25015, '413', 'Y', 'EST', '42.3773', '72.4671'),
(198, '01003', 'Amherst', 'MA', 'Hampshire', 25015, '413', 'Y', 'EST', '42.3752', '72.5212'),
(199, '01004', 'Amherst', 'MA', 'Hampshire', 25015, '413', 'Y', 'EST', '42.3736', '72.5209'),
(200, '01005', 'Barre', 'MA', 'Worcester', 25027, '978', 'Y', 'EST', '42.4202', '72.1060'),
(201, '01007', 'Belchertown', 'MA', 'Hampshire', 25015, '413', 'Y', 'EST', '42.2771', '72.4022'),
(202, '01008', 'Blandford', 'MA', 'Hampden', 25013, '413', 'Y', 'EST', '42.1887', '72.9556'),
(203, '01009', 'Bondsville', 'MA', 'Hampden', 25013, '413', 'Y', 'EST', '42.2075', '72.3496');

--
-- Constraints for dumped tables
--

--
-- Constraints for table `fooditems`
--
ALTER TABLE `fooditems`
  ADD CONSTRAINT `fooditems_ibfk_1` FOREIGN KEY (`SubItemTypeID`) REFERENCES `subitemtype` (`SubItemTypeID`);

--
-- Constraints for table `recipeitems`
--
ALTER TABLE `recipeitems`
  ADD CONSTRAINT `recipeitems_ibfk_1` FOREIGN KEY (`RecipeID`) REFERENCES `recipe` (`RecipeID`),
  ADD CONSTRAINT `recipeitems_ibfk_2` FOREIGN KEY (`ItemID`) REFERENCES `fooditems` (`ItemID`);

--
-- Constraints for table `subitemtype`
--
ALTER TABLE `subitemtype`
  ADD CONSTRAINT `subitemtype_ibfk_1` FOREIGN KEY (`ItemTypeID`) REFERENCES `itemtype` (`ItemTypeID`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
