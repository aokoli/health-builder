package edu.luc.etl.cs313.android.shapes.model;

/**
 * A graphical shape.
 */
public interface Shape {
	<Result> Result accept(Visitor<Result> v);  // TODO What is the function of the <Result> behind accept function? A cast to a generic return type? (since void, Integer etc. can be returned)
}
