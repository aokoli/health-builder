package edu.luc.etl.cs313.android.shapes.android;
/*
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyInt;

import java.util.ArrayList;
*/
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Paint.Style;
import edu.luc.etl.cs313.android.shapes.model.*;

/**
 * A Visitor for drawing a shape to an Android canvas.
 */
public class Draw implements Visitor<Void> {

	private final Canvas canvas;

	private final Paint paint;

	public Draw(final Canvas canvas, final Paint paint) {
		this.canvas = canvas; 
		this.paint = paint; 
		paint.setStyle(Style.STROKE);
	}

	@Override
	public Void onCircle(final Circle c) {
		canvas.drawCircle(0, 0, c.getRadius(), paint);
		return null;
	}

	@Override
	public Void onStroke(final Stroke c) {
		paint.setColor(c.getColor());
		c.getShape().accept(this);
		paint.setColor(-(c.getColor()));  
		return null;
	}

	@Override
	public Void onFill(final Fill f) {		
		paint.setStyle(Style.FILL_AND_STROKE);
		f.getShape().accept(this);
		return null;
	}

	@Override
	public Void onGroup(final Group g) {
		
		for (int i = 0; i < g.getShapes().size(); i++){
			g.getShapes().get(i).accept(this);
		}
		
		return null;
	}

	@Override
	public Void onLocation(final Location l) {
		
		canvas.translate(l.getX(), l.getY());
		l.getShape().accept(this); 
		paint.setStyle(Style.STROKE);
		paint.setColor(Color.BLACK); 
		canvas.translate(-(l.getX()), -(l.getY()));
		return null;
	}

	@Override
	public Void onRectangle(final Rectangle r) {
		canvas.drawRect(0, 0, r.getWidth(), r.getHeight(), paint);
		return null;
	}

	@Override
	public Void onOutline(Outline o) {
		paint.setStyle(Style.STROKE);
		o.getShape().accept(this);
		paint.setStyle(Style.STROKE);  
		return null;
	}

	@Override
	public Void onPolygon(final Polygon s) {
		
		final float[] pts = new float[(s.getPoints().size()) * 2]; 

		int j = 0;
		for (int i = 0; i < pts.length; i += 2){
			pts[i] = s.getPoints().get(j).getX();
			pts[i+1] = s.getPoints().get(j).getY();
			j++;
		}
		
		canvas.drawLines(pts, paint);
		canvas.drawLines(pts, 2, pts.length-4, paint);
		canvas.drawLine(pts[0], pts[1], pts[pts.length-2], pts[pts.length-1], paint); // final stroke
		
		return null;
	}
}
