package edu.luc.etl.cs313.android.shapes.model;

/**
 * A shape visitor for calculating the bounding box, that is, the smallest
 * rectangle containing the shape. The resulting bounding box is returned as a
 * rectangle at a specific location.
 */
public class BoundingBox implements Visitor<Location> {

	@Override
	public Location onCircle(final Circle c) {
		final int radius = c.getRadius();
		return new Location(-radius, -radius, new Rectangle(2 * radius, 2 * radius)); 
	}

	@Override
	public Location onFill(final Fill f) {
		Location b =  f.getShape().accept(this);
		return new Location(b.getX(), b.getY(), b.getShape());
	}

	@Override
	public Location onGroup(final Group g) {
		int largestX = Integer.MIN_VALUE;
		int largestY = Integer.MIN_VALUE;
		int smallestX =  Integer.MAX_VALUE;  
		int smallestY = Integer.MAX_VALUE; 
		
		for (int i = 0; i < g.getShapes().size(); i++){					
			final Location currentBox = g.getShapes().get(i).accept(this); 
			final int currentX = currentBox.getX(); 
			final int currentY = currentBox.getY();
			final int farthestX = currentBox.getX() + ((Rectangle) currentBox.getShape()).getWidth();	
			final int farthestY = currentBox.getY() + ((Rectangle) currentBox.getShape()).getHeight();  
			
			// Getting the highest x and y values	
			if (largestX < farthestX){
				largestX = farthestX;
			}
			if (largestY < farthestY){
				largestY = farthestY;
			}
			
			// Getting the lowest x and y values	
			if (smallestX > currentX){
				smallestX = currentX;
			}
			if (smallestY > currentY){
				smallestY = currentY;
			}
			
		}
		
		int groupShapeBoundingBoxWidth = largestX - smallestX;
		int groupShapeBoundingBoxHeight = largestY - smallestY;
		
		return new Location (smallestX, smallestY, new Rectangle(groupShapeBoundingBoxWidth, groupShapeBoundingBoxHeight));
		
	}
															
	@Override
	public Location onLocation(final Location l) {  
	
		Location b =  l.getShape().accept(this); 
		return new Location(l.getX() + b.getX(), l.getY() + b.getY(), b.getShape()); 
	} 

	@Override
	public Location onRectangle(final Rectangle r) {
		return new Location(0, 0, r);
	}

	@Override
	public Location onStroke(final Stroke c) {
		Location b =  c.getShape().accept(this);
		return new Location(b.getX(), b.getY(), b.getShape());
	}

	@Override
	public Location onOutline(final Outline o) {
		Location b =  o.getShape().accept(this);
		return new Location(b.getX(), b.getY(), b.getShape());
	}

	@Override
	public Location onPolygon(final Polygon s) {

		int highestX = Integer.MIN_VALUE;
		int highestY = Integer.MIN_VALUE;
		int lowestX = Integer.MAX_VALUE;
		int lowestY = Integer.MAX_VALUE;
		int currentX = s.getPoints().get(0).getX();
		int currentY = s.getPoints().get(0).getY();
		
		for (int i = 0; i < s.getPoints().size(); i++){
			
			currentX = s.getPoints().get(i).getX();
			currentY = s.getPoints().get(i).getY();
			
			// Getting the highest x and y values
			if (highestX < currentX){
				highestX = currentX;
			}
			if (highestY < currentY){
				highestY = currentY;
			}				
			
			// Getting the lowest x and y values
			if (lowestX > currentX){
				lowestX = currentX;
			}
			if (lowestY > currentY){
				lowestY = currentY;
			}			
		}
		
		// Calculating the height and width for bounding rectangle
		int rectangleWidth = highestX - lowestX;
		int rectangleHeight = highestY - lowestY;
		
		return new Location (lowestX, lowestY, new Rectangle(rectangleWidth, rectangleHeight));
	}
}
