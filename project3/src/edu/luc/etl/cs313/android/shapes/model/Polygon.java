package edu.luc.etl.cs313.android.shapes.model;

import java.util.List;

/**
 * A special case of a group consisting only of Points.
 *
 */
public class Polygon extends Group {

	public Polygon(final Point... points) {
		super(points);
	}

	@SuppressWarnings("unchecked")
	public List<? extends Point> getPoints() {  // TODO Why "? extends Point"? Why not just <Point>?
		return (List<? extends Point>) getShapes();
	}

	@Override
	public <Result> Result accept(final Visitor<Result> v) {
		return v.onPolygon(this);
	}
}
