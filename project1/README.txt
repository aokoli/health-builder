Responses for TODO's in TestPerformance.java file:

// TODO run test and record running times for SIZE = 10, 100, 1000, 10000
	// which of the two lists performs better as the size increases?

	/* RESPONSE: ArrayList performs faster as the size increases as shown below
	 * 
	 * _____________________________________SPEED TESTS_______________________________________________________
	 * SIZE  	|testLinkedListAddRemove | testArrayListAddRemove | testLinkedListAccess | testArrayListAccess
	 * 10	 	|	0.075s   	 |	  0.056s  	  |	   0.023s	 |	0.017s		
	 * 100	 	|	0.061s		 |	  0.078s	  |	   0.045s	 |	0.013s	
	 * 1000		|	0.055s		 |	  0.409s	  |        0.390s	 |	0.012s		  		
	 * 10000    	|	0.069s		 |	  4.228s	  |	   7.571s	 |	0.014s		  		
	 */



Responses for TODO's in TestIterator.java file:

	@Before
	public void setUp() throws Exception {
		list = new LinkedList<Integer>();

		// TODO also try with a LinkedList - does it make any difference?

		// RESPONSE: None of the methods fail when a LinkedList is used. It seemed
		// like the ArrayList was faster on average, but after conducting many tests,
		// both ArrayList and LinkedList appeared to average at around the same values.
	}




	@Test	
	public void testNonempty() {
		list.add(33);
		list.add(77);
		list.add(44);
		list.add(77);
		list.add(55);
		list.add(77);
		list.add(66);
		final Iterator<Integer> i = list.iterator();
		assertTrue(i.hasNext());
		assertEquals(33, i.next().intValue());

		// TODO fix the expected values in the assertions below

		assertTrue(i.hasNext());
		assertEquals(77, i.next().intValue());
		assertTrue(i.hasNext());
		assertEquals(44, i.next().intValue());
		assertTrue(i.hasNext());
		assertEquals(77, i.next().intValue());
		assertTrue(i.hasNext());
		assertEquals(55, i.next().intValue());
		assertTrue(i.hasNext());
		assertEquals(77, i.next().intValue());
		assertTrue(i.hasNext());
		assertEquals(66, i.next().intValue());
		assertFalse(i.hasNext());
	}



	@Test	
	public void testRemove() {
		list.add(33);
		list.add(77);
		list.add(44);
		list.add(77);
		list.add(55);
		list.add(77);
		list.add(66);
		final Iterator<Integer> i = list.iterator();
		while (i.hasNext()) {
			if (i.next() == 77) {
				i.remove(); // TODO what happens if you use list.remove(77)?

				// RESPONSE: The test would fail. list.remove(77) would be attempting
				// to remove the element at the 77th index position. Since
				// there are only 7 indexes, there would be an OutOfBoundsException.
			}
		}
		// TODO using assertEquals and Arrays.asList (see above)
		// express which values are left in the list
		assertEquals(Arrays.asList(33,44,55,66), list);
	}




	public void testAverageValues() {
		list.add(33);
		list.add(77);
		list.add(44);
		list.add(77);
		list.add(55);
		list.add(77);
		list.add(66);
		double sum = 0;
		int n = 0;
		
		// TODO use an iterator and a while loop to compute the average (mean) of the values
		// (defined as the sum of the items divided by the number of items)

		// RESPONSE:
		Iterator<Integer> i = list.iterator();  
		while(i.hasNext()) {
			sum += i.next().intValue();
			n++;
		}
		assertEquals(61.3, sum / n, 0.1);
		assertEquals(7, n);
	}
}



Responses for TODO's in TestList.java file:

	@Before
	public void setUp() throws Exception {
		list = new LinkedList<Integer>();

		// TODO also try with a LinkedList - does it make any difference?

		// RESPONSE: None of the methods fail when a LinkedList is used. There is 
		// very slight difference in run speed, but ArrayList was faster (the speed was
		// averaging at 0.001s). The speed of LinkedList version varied, though 
		// it was still slightly slower than the ArrayList (averaged at 0.003s). 
	}


	@Test	
	public void testSizeNonEmpty() {

		// TODO fix the expected values in the assertions below		
		
		// RESPONSE:
		list.add(77);
		assertEquals(false, list.isEmpty()); 
		assertEquals(1, list.size()); 
		assertEquals(77, list.get(0).intValue()); 
	}



	@Test	
	public void testContains() {
		// TODO write assertions using
		// list.contains(77)
		// that hold before and after adding 77 to the list

		assertEquals(true, list.isEmpty());
		list.add(77);
		list.contains(77);
		assertEquals(1, list.size());
		assertEquals(77, list.get(0).intValue());
	}



	@Test  
	public void testAddMultiple() {
		list.add(77);
		list.add(77);
		list.add(77);

		// TODO fix the expected values in the assertions below
		assertEquals(3, list.size());
		assertEquals(0, list.indexOf(77));
		assertEquals(77, list.get(1).intValue());
		assertEquals(2, list.lastIndexOf(77));
	}


	@Test	
	public void testAddMultiple2() {
		list.add(33);
		list.add(77);
		list.add(44);
		list.add(77);
		list.add(55);
		list.add(77);
		list.add(66);
		// TODO fix the expected values in the assertions below
		assertEquals(7, list.size());
		assertEquals(1, list.indexOf(77));
		assertEquals(5, list.lastIndexOf(77));
		assertEquals(44, list.get(2).intValue());
		assertEquals(77, list.get(3).intValue());
		assertEquals(Arrays.asList(33, 77, 44, 77, 55, 77, 66), list);
	}



	@Test	
	public void testRemoveObject() {
		list.add(3);
		list.add(77);
		list.add(4);
		list.add(77);
		list.add(5);
		list.add(77);
		list.add(6);
		list.remove(5); // what does this method do? RESPONSE: It removes the element
		// at index 5 (reducing list size) i.e. it removes 77, and shifts subsequent 
		// elements left. 

		// TODO fix the expected values in the assertions below

		assertEquals(6, list.size());
		assertEquals(1, list.indexOf(77));
		assertEquals(3, list.lastIndexOf(77));
		assertEquals(4, list.get(2).intValue());
		assertEquals(77, list.get(3).intValue());
		list.remove(Integer.valueOf(5)); // what does this one do? RESPONSE: It 
		// removes the element containing the first occurrence of the number 5 i.e.
		// it removes element at index 4, shifting the subsequent elements left. 

		assertEquals(5, list.size());
		assertEquals(1, list.indexOf(77));
		assertEquals(3, list.lastIndexOf(77));
		assertEquals(4, list.get(2).intValue());
		assertEquals(77, list.get(3).intValue());
	}



	@Test	
	public void testContainsAll() {
		list.add(33);
		list.add(77);
		list.add(44);
		list.add(77);
		list.add(55);
		list.add(77);
		list.add(66);
		// TODO using containsAll and Arrays.asList (see above),
		// 1) assert that list contains all five different numbers added
		// 2) assert that list does not contain all of 11, 22, and 33
		assertEquals(Arrays.asList(33, 77, 44, 77, 55, 77, 66), list);
		assertEquals(true, !list.contains(11));
		assertEquals(true, !list.contains(22));
		assertEquals(false, !list.contains(33));
		
	}



	@Test	
	public void testAddAll() {
		// TODO in a single statement using addAll and Arrays.asList,
		// add items to the list to make the following assertions pass
		// (without touching the assertions themselves)
		list.addAll(Arrays.asList(33,77,44,77,55,77,66));
		assertEquals(7, list.size());
		assertEquals(33, list.get(0).intValue());
		assertEquals(77, list.get(1).intValue());
		assertEquals(44, list.get(2).intValue());
		assertEquals(77, list.get(3).intValue());
		assertEquals(55, list.get(4).intValue());
		assertEquals(77, list.get(5).intValue());
		assertEquals(66, list.get(6).intValue());
	}



	@Test	
	public void testRemoveAll() {
		list.add(33);
		list.add(77);
		list.add(44);
		list.add(77);
		list.add(55);
		list.add(77);
		list.add(66);
		// TODO in a single statement using removeAll and Arrays.asList,
		// remove items from the list to make the following assertions pass
		// (without touching the assertions themselves)
		list.removeAll(Arrays.asList(33,44,55,66));
		assertEquals(3, list.size());
		assertEquals(Arrays.asList(77, 77, 77), list);
	}



	@Test	
	public void testRetainAll() {
		list.add(33);
		list.add(77);
		list.add(44);
		list.add(77);
		list.add(55);
		list.add(77);
		list.add(66);
		// TODO in a single statement using retainAll and Arrays.asList,
		// remove items from the list to make the following assertions pass
		// (without touching the assertions themselves)
		list.retainAll(Arrays.asList(77));
		assertEquals(3, list.size());
		assertEquals(Arrays.asList(77, 77, 77), list);
	}


	
	@Test	
	public void testSet() {
		list.add(33);
		list.add(77);
		list.add(44);
		list.add(77);
		list.add(55);
		list.add(77);
		list.add(66);
		// TODO use the set method to change specific elements in the list
		// such that the following assertions pass
		// (without touching the assertions themselves)
		list.set(1, 99);
		list.set(3, 99);
		list.set(5, 99);
		assertEquals(7, list.size());
		assertEquals(33, list.get(0).intValue());
		assertEquals(99, list.get(1).intValue());
		assertEquals(44, list.get(2).intValue());
		assertEquals(99, list.get(3).intValue());
		assertEquals(55, list.get(4).intValue());
		assertEquals(99, list.get(5).intValue());
		assertEquals(66, list.get(6).intValue());
	}


	@Test	
	public void testSubList() {
		list.add(33);
		list.add(77);
		list.add(44);
		list.add(77);
		list.add(55);
		list.add(77);
		list.add(66);
		// TODO fix the arguments in the subList method so that the assertion
		// passes
		assertEquals(Arrays.asList(44, 77, 55), list.subList(2, 5));
	}
