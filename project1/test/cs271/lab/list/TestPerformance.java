package cs271.lab.list;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class TestPerformance {

	// TODO run test and record running times for SIZE = 10, 100, 1000, 10000
	// which of the two lists performs better as the size increases?
	/* RESPONSE: ArrayList performs faster as the size increases as shown below
	 * 
	 * _____________________________________SPEED TESTS_______________________________________________________
	 * SIZE  	|testLinkedListAddRemove | testArrayListAddRemove | testLinkedListAccess | testArrayListAccess
	 * 10	 	|		0.075s   		 |		  0.056s		  |		0.023s		  	 |		0.017s		
	 * 100	 	|		0.061s			 |		  0.078s		  |		0.045s			 |		0.013s	
	 * 1000		|		0.055s			 |		  0.409s		  |		0.390s			 |		0.012s		  		
	 * 10000    |		0.069s			 |		  4.228s		  |		7.571s			 |		0.014s		  		
	 */
	private final int SIZE = 10;

	private final int REPS = 1000000;

	private List<Integer> arrayList;

	private List<Integer> linkedList;

	@Before
	public void setUp() throws Exception {
		arrayList = new ArrayList<Integer>(SIZE);
		linkedList = new LinkedList<Integer>();
		for (int i = 0; i < SIZE; i++) {
			arrayList.add(i);
			linkedList.add(i);
		}
	}

	@After
	public void tearDown() throws Exception {
		arrayList = null;
		linkedList = null;
	}

	@Test
	public void testLinkedListAddRemove() {
		for (int r = 0; r < REPS; r++) {
			linkedList.add(0, 77);
			linkedList.remove(0);
		}
	}

	@Test
	public void testArrayListAddRemove() {
		for (int r = 0; r < REPS; r++) {
			arrayList.add(0, 77);
			arrayList.remove(0);
		}
	}

	@Test
	public void testLinkedListAccess() {
		@SuppressWarnings("unused")
		long sum = 0;
		for (int r = 0; r < REPS; r++) {
			sum += linkedList.get(r % SIZE);
		}
	}

	@Test
	public void testArrayListAccess() {
		@SuppressWarnings("unused")
		long sum = 0;
		for (int r = 0; r < REPS; r++) {
			sum += arrayList.get(r % SIZE);
		}
	}
}
