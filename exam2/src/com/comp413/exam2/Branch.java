package com.comp413.exam2;

import java.util.Collection;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

public class Branch implements Tree {
	private final List<Tree> children = new LinkedList<Tree>();
	public List<Tree> getChildren() { return Collections.unmodifiableList(children); }
	public void addChild(final Tree child) { children.add(child); }
	
	
	@Override
	public int size() {
		int size = 0;
		for (Tree tree : children){
			size += tree.size();
		}
		return size;
	}
	@Override
	public void grow(int factor) {
		for (Tree tree : children) {
			tree.grow(factor);
		}
		
		/*
		// Get original number of trees 
					int treeNum = children.size();
					// Grow each tree by factor
					for (int i = 0; i < treeNum; i++) {
						for (int j = 1; j < factor; j++) {
							// Duplicate tree
							Tree tree = children.get(i);
							children.add(tree);
						}
					}*/
	}
	@Override
	public void harvest(Collection<Fruit> bag) {
		for (Tree tree : children) {
			tree.harvest(bag);
		}
	}
	
	
}
