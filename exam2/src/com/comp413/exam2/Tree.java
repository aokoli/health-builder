package com.comp413.exam2;

import java.util.Collection;

public interface Tree {
	int size(); // total number of fruit in the tree
	void grow(int factor); // grows the number of fruit by given factor
	void harvest(Collection<Fruit> bag); // harvests all fruits in this tree into bag
	

}
