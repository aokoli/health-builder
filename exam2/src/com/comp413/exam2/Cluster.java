package com.comp413.exam2;

import java.util.Collection;

public class Cluster implements Tree { 
	private int howmany;
	private Tree child;
	public void setMultiplier(final int howmany) { this.howmany = howmany; }
	public int getMultiplier() { return howmany; }
	public void setChild(final Tree child) { this.child = child; }
	public Tree getChild() { return child; }
	
	
	public Cluster(int multiplier, Tree tree) {
		this.howmany = multiplier;
		this.child = tree;
	}
	
	@Override
	public int size() {
		return howmany * child.size(); // growth factor * multiplier * child.size()
	}

	@Override
	public void grow(int factor) {

		howmany *= factor;
		
		for (int i = 0; i < howmany; i++) {
			child.grow(factor); 
		}
		
		// TODO So if there are 5 mangoes, cluster(5, Mango())
		/*
		 * size() = 5 mangoes
		 * harvest() = 5 mangoes
		 * 
		 * Since individual mango trees can't grow, where will the 5 mangoes from above be stored? In cluster
		 * class? Do we create a new List to store all 5 mangoes within cluster class?
		 * OR WILL THEY EVEN BE STORED AT ALL?
		 * 
		 * Also, if grow(1), still yields 10, and grow(2) = 20. Will a grow(1) after that yield 10? Or 20? <-- 20. Since it's multiplicative?
		 * 
		 */
		
	}

	@Override
	public void harvest(Collection<Fruit> bag) {
		//child.harvest(bag);  // TODO perform 5 times?
		
		for (int i = 0; i < howmany; i++) {
			child.harvest(bag);  // TODO Correct?
		}
	}
	
}
