package com.comp413.exam2;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public abstract class Fruit implements Tree {
	
	@Override
	public int size() {
		return 1;
	}

	@Override
	public void grow(int factor) { /* Nothing to do here */ }

	@Override
	public void harvest(Collection<Fruit> bag) {
		bag.add(this);
	}

	
}
