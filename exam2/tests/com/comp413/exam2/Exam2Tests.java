package com.comp413.exam2;

import static org.junit.Assert.*;
import static org.junit.Assert.assertEquals;

import java.util.LinkedList;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class Exam2Tests {

	Branch trunk, mangoBranch, branchOne, branchTwo ;
	Cluster mangoCluster, peachCluster;
	Fruit mango1 = new Mango();
	Fruit mango2 = new Mango();
	Fruit mango3 = new Mango();
	Fruit mango4 = new Mango();
	Fruit peach = new Peach();

	@Before
	public void setUp() throws Exception {
		
		trunk = new Branch();  // Main branch
		
		branchOne = new Branch(); // Create first sub-branch
		branchOne.addChild(mango1);  // Add 1st mango
		branchOne.addChild(mango2);  // Add 2nd mango
		
		branchTwo = new Branch(); // Create second sub-branch
		mangoBranch = new Branch();
		mangoBranch.addChild(mango3);		// Add 1st mango
		mangoBranch.addChild(mango4);		// Add 2nd mango
        mangoCluster = new Cluster(1, mangoBranch); // Creates a cluster of two mangoes
		branchTwo.addChild(mangoCluster);	    // Adds mangoCluster to branchTwo
		peachCluster = new Cluster(1, peach);   // Creates a cluster of one peach
		branchTwo.addChild(peachCluster); // Adds peach cluster to branchTwo

		trunk.addChild(branchOne);  // Adds branchOne to the trunk
		trunk.addChild(branchTwo); // Adds branchTwo to the trunk
	}

	@After
	public void tearDown() throws Exception {
		trunk = null;
	}
	
	@Test
	public void test3e() {
		assertEquals(5, trunk.size());
	}
	
	@Test
	public void test3f() {
		// Bag to be used for harvesting fruits
		final List<Fruit> bag = new LinkedList<Fruit>();
		// Assembly of all the fruits that were originally planted
		final List<Fruit> originalFruits = new LinkedList<Fruit>();

		// Creating inventory of original fruits planted
		originalFruits.add(mango1);
		originalFruits.add(mango2);
		originalFruits.add(mango3);
		originalFruits.add(mango4);
		originalFruits.add(peach);
		
		// Harvest all fruits planted into bag
		trunk.harvest(bag);
		
		// Verifies that bag of harvest contains all original fruits planted
		assertTrue(bag.containsAll(originalFruits));

	}
	
	@Test
	public void test3g() {
		trunk.grow(3);
		// There are 2 clusters - one contains 2 mangoes ("mango cluster"), and 
		// the other contains 1 peach ("peach cluster"). Each cluster has a multiplier of 1.
		// Hence, a growth factor of 3 on the trunk would yield 11, as shown below:
		// Mango cluster: 3 (factor) * 1 (multiplier) * 2 (size) +
		// Peach cluster: 3 (factor) * 1 (multiplier) * 1 (size) +
		// 2 mangoes (no cluster): 2 
		//
		assertEquals(11, trunk.size());
	
	}

}
