package com.okolialex.alextimer.model.time;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import static com.okolialex.alextimer.common.Constants.*;

import org.junit.Test;

import com.okolialex.alextimer.model.time.TimeModel;

/**
 * Testcase superclass for the time model abstraction.
 * This is a simple unit test of an object without dependencies.
 *
 * @author Alexander Okoli
 * @see http://xunitpatterns.com/Testcase%20Superclass.html
 */
public abstract class AbstractTimeModelTest {

	private TimeModel model;

	/**
	 * Setter for dependency injection. Usually invoked by concrete testcase
	 * subclass.
	 *
	 * @param model
	 */
	protected void setModel(final TimeModel model) {
		this.model = model;
	}

	/**
	 * Verifies that current displayed time and idle time are initially 0.
	 */
	@Test
	public void testPreconditions() {
		assertEquals(0, model.getCurrentTime());
		assertEquals(0, model.getIdleTime());
	}
	
	/**
	 * Verifies the following: 
	 * Idle time is incremented correctly; idle time reaches max limit; idle time 
	 * is reset correctly.
	 */
	@Test
	public void testIdleTime() {
		assertEquals(0, model.getIdleTime());
		// Idle time is incremented correctly
		for (int i = 0; i < MAX_IDLE_TIME; i++){
			model.incIdleTime();
		}
		assertEquals(3, model.getIdleTime());
		
		// Model recognizes when the idle time has reached max limit
		assertTrue(model.isIdleTimeMaxed());
		
		// Idle time can be reset to 0
		model.resetIdleTime();
		assertEquals(0, model.getIdleTime());	
	}
	

	/**
	 * Verifies the following:
	 * Current displayed time is incremented correctly; the current time can be reset;
	 * the maximum time that can be set (99) is not exceeded; the minimum time that 
	 * can be displayed (0) is accurate.
	 */
	@Test
	public void testCurrentTime() {
		assertEquals(0, model.getCurrentTime());
		// Current time increments correctly
		for (int i = 0; i < 10; i++) {
			model.incCurrentTime();
		}
		assertEquals(10, model.getCurrentTime());
		
		// Current time can be reset to 0
		model.resetCurrentTime();
		assertEquals(0, model.getCurrentTime());
		
		// Maximum set-able time is 99
		while (!model.isFull()) {
			model.incCurrentTime();
		}
		assertEquals(99, model.getCurrentTime());
		
		// Minimum time model can be decremented to is 0
		while (!model.isEmpty()) {
			model.decCurrentTime();			
		}
		assertEquals(0, model.getCurrentTime());
	}
}

