package com.okolialex.alextimer.model.state;

import org.junit.After;
import org.junit.Before;

/**
 * Concrete testcase subclass for the default AlexTimer state machine
 * implementation.
 *
 * @author Alexander Okoli
 * @see http://xunitpatterns.com/Testcase%20Superclass.html
 */
public class DefaultAlexTimerStateMachineTest extends AbstractAlexTimerStateMachineTest {

	@Before
	public void setUp() throws Exception {
		super.setUp();
		setModel(new DefaultAlexTimerStateMachine(getDependency(), getDependency()));
	}

	@After
	public void tearDown() {
		setModel(null);
		super.tearDown();  
	}
}