package com.okolialex.alextimer.android;

import java.io.IOException;

import android.app.Activity;
import android.content.Context;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.okolialex.alextimer.android.R;
import com.okolialex.alextimer.common.AlexTimerUIUpdateListener;
import com.okolialex.alextimer.model.AlexTimerModelFacade;
import com.okolialex.alextimer.model.ConcreteAlexTimerModelFacade;

/**
 * A thin adapter component for the AlexTimer.
 *
 * @author Alexander Okoli
 */
public class AlexTimerAdapter extends Activity implements AlexTimerUIUpdateListener {
		
	private MediaPlayer mediaPlayer;
	
	private AlexTimerModelFacade model;
	
	/**
	 * Setter for the state-based dynamic model.
	 * 
	 * @param model
	 */
	protected void setModel(final AlexTimerModelFacade model) {
		this.model = model;
	}
	
	/**
	 * Setter for the media player
	 * 
	 * @param mediaPlayer
	 */
	protected void setMediaPlayer(final MediaPlayer mediaPlayer) {
		this.mediaPlayer = mediaPlayer;
	}
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		// inject dependency on view so this adapter receives UI events
		setContentView(R.layout.activity_main);  
		
		AlexTimerAdapter savedAdapter = (AlexTimerAdapter) getLastNonConfigurationInstance();
		// Checks if an AlexTimerAdapter object was saved upon configuration change e.g. device rotation 
		if (savedAdapter != null){
			// Restores all the previous states from the saved AlexTimerAdapter
			setModel(savedAdapter.model);
			setMediaPlayer(savedAdapter.mediaPlayer);
		} else { 
			// Creates new instance of model
			setModel(new ConcreteAlexTimerModelFacade());
		}
		 model.setUIUpdateListener(this);
	}
	
	/**
	 * Preserves the model state for configuration changes such as device rotation.
	 */
	@Override
	public Object onRetainNonConfigurationInstance() {
		final AlexTimerAdapter savedAdapter = this;
		return savedAdapter;
	    
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}
	
	@Override
	protected void onStart() {
		super.onStart();
		/*
		 *  If model.onStart() is triggered, state machine goes to Stopped state. Upon a config change (device rotation), 
		 *  a version of the model is saved. If the saved model was, say, in the Running state, the onTick() from that 
		 *  saved model will still be active/firing and this would ultimately crash the app (since the app will try call 
		 *  onTick() in the Stopped state, which is currently programmed to throw an Exception). So per below, model.onStart() 
		 *  should not be re-triggered if there is a saved version of the model.
		 */
		if (getLastNonConfigurationInstance() == null){ 
			model.onStart(); 
		}
	}
	
	/**
	 * Plays the default alarm sound.
	 */
	@Override
	public void playDefaultAlarm() {
		final Uri defaultRingtoneUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_ALARM);
		setMediaPlayer(new MediaPlayer());
		final Context context = getApplicationContext();

		try {
			mediaPlayer.setDataSource(context, defaultRingtoneUri);
			mediaPlayer.setAudioStreamType(AudioManager.STREAM_ALARM);
			mediaPlayer.prepare();
			mediaPlayer.setLooping(true);
			mediaPlayer.start();
		} catch (final IOException ex) {
			throw new RuntimeException(ex);
		}
	} 
	
	/**
	 * Stops the default alarm sound
	 */
	@Override
	public void stopDefaultAlarm(){
		mediaPlayer.setLooping(false);  // TODO Might be unneeded.
		mediaPlayer.stop();
		mediaPlayer.release();
	}

	/**
	 * Updates the time (seconds) in the UI.
	 * 
	 * @param time
	 */
	@Override
	public void updateTime(final int time) {
		final TextView tvS = (TextView) findViewById(R.id.seconds);
		tvS.setText(Integer.toString(time / 10) + Integer.toString(time % 10));	
	}

	/**
	 * Updates the state name in the UI.
	 * 
	 * @param stateId
	 */
	@Override
	public void updateState(final int stateId) {
		final TextView stateName = (TextView) findViewById(R.id.stateName);
		stateName.setText(getString(stateId));		
	}

	/**
	 * Updates the button name in the UI.
	 * 
	 * @param buttonId
	 */
	@Override
	public void updateButtonName(final int buttonId) {
		final TextView stateName = (TextView) findViewById(R.id.timerButton);
		stateName.setText(getString(buttonId));	
	}
	
	/**
	 * Updates the button accessibility in the UI.
	 * 
	 * @param buttonId
	 */
	@Override
	public void updateButtonAccess(boolean isFull) {
		((Button) findViewById(R.id.timerButton)).setEnabled(!isFull);		
	}
	
	// Event listener method forwarded to the current state
	public void onButtonPush(final View view) { model.onButtonPush(); }



}
