package com.okolialex.alextimer.common;

/**
 * A source of UI update events for the AlexTimer. 
 * This interface injects the Adapter (or any listener object) into the model in order
 * for the Adapter to listen to the model - becoming "aware" of model notifications. 
 * Upon listening to model, Adapter methods for updating the UI are ultimately triggered. 
 * This interface is typically implemented by the model.
 *
 * @author Alexander Okoli
 */

public interface AlexTimerUIUpdateAware {
	void setUIUpdateListener(AlexTimerUIUpdateListener uiUpdateListener);
}
