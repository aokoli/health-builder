package com.okolialex.alextimer.common;

/**
 * This class contains all constant values needed for AlexTimer to function. 
 * 
 * @author Alexander Okoli
 *
 */
public final class Constants {
	
	public static int SEC_PER_TICK = 1;
	public static int TIME_INC_PER_CLICK = 1; 
	public static int MAX_IDLE_TIME = 3; 
	public static int MAX_TIME_SETTABLE = 99;
	public static int MIN_TIMER_TIME = 0;

	private Constants() { }

}
