package com.okolialex.alextimer.common;

/**
 * This is a listener for AlexTimer events (i.e. button presses) coming from UI.
 * 
 * @author Alexander Okoli
 */

public interface AlexTimerUIListener {
	public void onButtonPush();
}
