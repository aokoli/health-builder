package com.okolialex.alextimer.common;

/**
 * A listener for UI update notifications.
 * This interface is typically implemented by the adapter, with the notifications 
 * coming from the model. Upon listening to notifications from the model, this interface 
 * performs appropriate actions on the UI. 
 *
 * @author Alexander Okoli
 */

public interface AlexTimerUIUpdateListener {
	public void updateTime(int time);
	public void updateState(int stateId);
	public void updateButtonName(int buttonNameId);
	public void updateButtonAccess(boolean isFull);
	public void playDefaultAlarm();
	public void stopDefaultAlarm();
}
