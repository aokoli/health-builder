package com.okolialex.alextimer.model;

/**
 * An implementation of the model facade.
 * 
 * @author Alexander Okoli 
 */
import com.okolialex.alextimer.common.AlexTimerUIUpdateListener;
import com.okolialex.alextimer.model.clock.ClockModel;
import com.okolialex.alextimer.model.clock.DefaultClockModel;
import com.okolialex.alextimer.model.clock.HandlerAsRunnableScheduler;
import com.okolialex.alextimer.model.state.AlexTimerStateMachine;
import com.okolialex.alextimer.model.state.DefaultAlexTimerStateMachine;
import com.okolialex.alextimer.model.time.DefaultTimeModel;
import com.okolialex.alextimer.model.time.TimeModel;

public class ConcreteAlexTimerModelFacade implements AlexTimerModelFacade {

	private AlexTimerStateMachine stateMachine;

	private ClockModel clockModel;

	private TimeModel timeModel;

	public ConcreteAlexTimerModelFacade() {
		timeModel = new DefaultTimeModel();
		clockModel = new DefaultClockModel();
		stateMachine = new DefaultAlexTimerStateMachine(timeModel, clockModel);
		clockModel.setOnTickListener(stateMachine);
	}
	
	@Override
	public void setUIUpdateListener(AlexTimerUIUpdateListener uiUpdateListener) {
		stateMachine.setUIUpdateListener(uiUpdateListener);		
	}
	
	@Override
	public void onStart() {
		clockModel.setRunnableScheduler(new HandlerAsRunnableScheduler());  
		stateMachine.actionInit(); 
	}
	
	@Override
	public void onButtonPush() {
		stateMachine.onButtonPush();		
	}
}
