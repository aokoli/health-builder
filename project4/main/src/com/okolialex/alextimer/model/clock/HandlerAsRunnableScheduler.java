package com.okolialex.alextimer.model.clock;

import android.os.Handler;

/**
 * Thin adapter for using a Handler as a RunnableScheduler.
 *
 * @author Alexander Okoli
 */
public class HandlerAsRunnableScheduler implements RunnableScheduler {

	private Handler handler = new Handler();

	@Override
	public boolean post(Runnable r) {
		return handler.post(r);
	}
}

