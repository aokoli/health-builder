package com.okolialex.alextimer.model.clock;

/**
 * A listener for onTick events coming from the internal clock model.
 *
 * @author Alexander Okoli
 */
public interface OnTickListener {
	public void onTick();
}
