package com.okolialex.alextimer.model.clock;

/**
 * An after-the-fact abstraction of {@link android.os.Handler}.
 * This is required for testing components that depend on a
 * message scheduler outside of the Android platform.   
 *
 * @author Alexander Okoli
 */
public interface RunnableScheduler {
	boolean post(Runnable r);
}
