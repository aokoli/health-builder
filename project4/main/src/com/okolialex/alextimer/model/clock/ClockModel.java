package com.okolialex.alextimer.model.clock;

/**
 * The active model of the internal clock that periodically emits tick events.
 *
 * @author Alexander Okoli
 */
public interface ClockModel {
	void setRunnableScheduler(RunnableScheduler scheduler);
	void setOnTickListener(OnTickListener listener);
	void start();
	void stop();
}

