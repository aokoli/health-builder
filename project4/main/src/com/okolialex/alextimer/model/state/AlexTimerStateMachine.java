package com.okolialex.alextimer.model.state;

import com.okolialex.alextimer.common.AlexTimerUIListener;
import com.okolialex.alextimer.common.AlexTimerUIUpdateAware;
import com.okolialex.alextimer.model.clock.OnTickListener;

public interface AlexTimerStateMachine extends AlexTimerSMStateView,
		AlexTimerUIListener, OnTickListener, AlexTimerUIUpdateAware { 
	public boolean isIdleTimeMaxed();
	public boolean isEmpty();
}
