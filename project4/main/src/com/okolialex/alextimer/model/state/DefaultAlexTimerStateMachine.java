package com.okolialex.alextimer.model.state;

import com.okolialex.alextimer.common.AlexTimerUIUpdateListener;
import com.okolialex.alextimer.model.clock.ClockModel;
import com.okolialex.alextimer.model.time.TimeModel;

/**
 * An implementation of the state machine for the AlexTimer.
 *
 * @author Alexander Okoli
 */
public class DefaultAlexTimerStateMachine implements AlexTimerStateMachine {
	
	private AlexTimerState state;
	
	private AlexTimerUIUpdateListener uiUpdateListener;
	
	private final TimeModel timeModel;

	private final ClockModel clockModel;
	

	public DefaultAlexTimerStateMachine (final TimeModel timeModel, final ClockModel clockModel) {
		this.timeModel = timeModel;
		this.clockModel = clockModel;
	}
	
	/**
	 * The internal state of this adapter component. Required for the State pattern.
	 */
	protected void setState(final AlexTimerState state) {
		this.state = state;
		actionUpdateEntireUI();
	}
	
	@Override 
	public void setUIUpdateListener(AlexTimerUIUpdateListener uiUpdateListener) {
		this.uiUpdateListener = uiUpdateListener;

		if (state != null) { actionUpdateEntireUI(); } /*The enclosed statement would be reached during a config 
		 * change (device rotation). It is needed because setContentView() in onCreate() resets the UI during a device
		 * rotation, displaying wrong data on the screen. The actionUpdateEntireUI() is called here because this is the 
		 * fastest access to the model on a device rotation, quickly reverting the UI back to its rightful display 
		 * (otherwise, the user will witness wrong data on the screen upon a device rotation for at least a small period of time).
		 */
	}
	
	// Events 
	@Override public void onButtonPush() { state.onButtonPush(); }
	@Override public void onTick() { state.onTick(); }
	
	// States
	private final AlexTimerState STOPPED  = new StoppedState(this);
	private final AlexTimerState TIME_SET = new TimeSetState(this);
	private final AlexTimerState RUNNING  = new RunningState(this);
	private final AlexTimerState ALARM    = new AlarmState(this);
	
	// Transitions
	@Override public void toStoppedState() { setState(STOPPED); }
	@Override public void toTimeSetState() { setState(TIME_SET); }
	@Override public void toRunningState() { setState(RUNNING); }
	@Override public void toAlarmState()   { setState(ALARM); }
	
	// Actions
	@Override public void actionInit() 		   	       { toStoppedState(); actionResetCurrentTime(); } 
	@Override public void actionResetCurrentTime() 	   { timeModel.resetCurrentTime(); actionUpdateUITime(); }
	@Override public void actionStartClock() 		   { clockModel.start(); }
	@Override public void actionStopClock() 		   { clockModel.stop(); }
	@Override public void actionStartAlarm() 		   { uiUpdateListener.playDefaultAlarm(); }
	@Override public void actionStopAlarm()			   { uiUpdateListener.stopDefaultAlarm(); }
	@Override public void actionIncIdleTime()    	   { timeModel.incIdleTime(); }
	@Override public void actionResetIdleTime()        { timeModel.resetIdleTime(); }
	@Override public void actionDecCurrentTime() 	   { timeModel.decCurrentTime(); actionUpdateUITime(); }
	@Override public void actionIncCurrentTime()	   { timeModel.incCurrentTime(); actionUpdateUITime(); }
	@Override public void actionUpdateUITime()  	   { uiUpdateListener.updateTime(timeModel.getCurrentTime()); 
													     uiUpdateListener.updateButtonAccess(timeModel.isFull()); }
	@Override public void actionUpdateEntireUI()  	   { actionUpdateUITime();  // Updates the current time in UI (It makes sense to give Time its own "actionUpdate" method as it frequently updated, relative to the button text/state banner in UI)
														 uiUpdateListener.updateState(state.getStateId());  // Updates state name in UI (banner in UI displaying current state)
														 uiUpdateListener.updateButtonName(state.getButtonId()); } // Updates button name in UI (button text in UI varies depending on state)

	// Helper methods to check climate of state machine and report to state
	@Override public boolean isIdleTimeMaxed() { return timeModel.isIdleTimeMaxed(); } 
	@Override public boolean isEmpty() { return timeModel.isEmpty(); }  	

	
	
}
