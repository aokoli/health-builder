package com.okolialex.alextimer.model.state;

import com.okolialex.alextimer.android.R;

/**
 * The Stopped state in the state machine. There are no
 * "moving" parts in this state and clock ticks are supposed to be at a halt.
 * 
 * @author Alexander Okoli
 *
 */
public class StoppedState implements AlexTimerState {

	AlexTimerStateMachine sm;
	
	public StoppedState(AlexTimerStateMachine sm) {
		this.sm = sm;
	}

	@Override
	public void onButtonPush() {
		sm.actionStartClock();
		sm.actionIncCurrentTime();
		sm.toTimeSetState();
	}

	@Override
	public void onTick() {
		 throw new UnsupportedOperationException("onTick");  
	}

	@Override
	public int getStateId() {
		return R.string.STOPPED;
	}

	@Override
	public int getButtonId() {
		return R.string.set_time_button;
	}

}
