package com.okolialex.alextimer.model.state;

import com.okolialex.alextimer.android.R;

/**
 * The Running state in the state machine. It is reached when
 * the Time Set state has expired, and the count down begins. 
 * 
 * @author Alexander Okoli
 *
 */
public class RunningState implements AlexTimerState {

	AlexTimerStateMachine sm;
	
	public RunningState(AlexTimerStateMachine sm) {
		this.sm = sm;
	}

	@Override
	public void onButtonPush() {
		sm.actionStopClock();
		sm.actionResetCurrentTime();
		sm.actionResetIdleTime();
		sm.toStoppedState();
	}

	@Override
	public void onTick() {
	    sm.actionDecCurrentTime();
	    if (sm.isEmpty()) { sm.toAlarmState(); }
	}

	@Override
	public int getStateId() {
		return R.string.RUNNING;
	}

	@Override
	public int getButtonId() {
		return R.string.cancel_button;
	}

}
