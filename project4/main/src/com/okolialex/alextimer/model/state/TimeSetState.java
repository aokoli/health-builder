package com.okolialex.alextimer.model.state;

import com.okolialex.alextimer.android.R;

/**
 * The Time Set state in the state machine. It is reached when
 * the Stopped state expires as the UI button is initially pushed. 
 * 
 * @author Alexander Okoli
 *
 */
public class TimeSetState implements AlexTimerState {

	AlexTimerStateMachine sm;
	
	public TimeSetState(AlexTimerStateMachine sm) {
		this.sm = sm;
	}

	@Override
	public void onButtonPush() {
		sm.actionIncCurrentTime();
		sm.actionResetIdleTime();
	}

	@Override
	public void onTick() {
	    sm.actionIncIdleTime();
		if (sm.isIdleTimeMaxed()) { sm.toRunningState(); };
	}

	@Override
	public int getStateId() {
		return R.string.TIME_SET;
	}

	@Override
	public int getButtonId() {
		return R.string.set_time_button;
	}

}
