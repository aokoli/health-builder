package com.okolialex.alextimer.model.state;

import com.okolialex.alextimer.common.AlexTimerUIListener;
import com.okolialex.alextimer.model.clock.OnTickListener;

/**
 * A state in a state machine. This interface is part of the State pattern.
 *
 * @author Alexander Okoli
 */
public interface AlexTimerState extends AlexTimerUIListener, OnTickListener {
	public int getStateId();
	public int getButtonId();
}
