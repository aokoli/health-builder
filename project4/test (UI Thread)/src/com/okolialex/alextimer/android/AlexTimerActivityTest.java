package com.okolialex.alextimer.android;

import android.test.ActivityInstrumentationTestCase2;

import android.widget.Button;
import android.widget.TextView;

import com.okolialex.alextimer.android.AlexTimerAdapter;
import com.okolialex.alextimer.android.R;

/**
 * A GUI-level test for several AlexTimer scenarios. 
 * 
 * @author Alexander Okoli
 *
 */
public class AlexTimerActivityTest extends
		ActivityInstrumentationTestCase2<AlexTimerAdapter> {
	
	AlexTimerAdapter mActivity; 
	private long actualTestDuration = 0;
	private final String STOPPED_STATE = "Stopped";
	private final String RUNNING_STATE = "Running";
	private final String TIME_SET_STATE = "Time Set";
	private final String ALARM_STATE = "Alarm";
	private final int IDLE_TIME = 3;
	private final int NUM_OF_BUTTON_CLICKS = 2;
	private final int EXPECTED_SET_TIME = NUM_OF_BUTTON_CLICKS;
	private final long EXPECTED_TEST_DURATION = EXPECTED_SET_TIME + IDLE_TIME;
	private final long DELTA = 500; // <-- (Minimum of 100). DELTA is needed because AlexTimer's 
	// "setup" time makes it slower by a fraction of a second. DELTA accounts for delay to make time tests work.
	// TODO UPDATE: "DELTA" is meant to be initial 1 second delay before timer's continuous ticks.
	
	
	public AlexTimerActivityTest() {
	    super(AlexTimerAdapter.class);
	}
	
	/**
	 * Creates an instance of the {@link AlexTimerAdapter} Activity prior to each unit test.
	 */
	@Override
	protected void setUp() throws Exception {
	    super.setUp();
	    setActivityInitialTouchMode(false);
	    mActivity = getActivity();
	}
	
	
	/**
	 * Verifies that the activity under test can be launched.
	 */
	public void testActivityTestCaseSetUpProperly() {
		assertNotNull("activity should be launched successfully", mActivity);
	}
	
	/**
	 * Verifies the following scenario: Time is 0, button is pressed 2 times and released, 
	 * 2 is displayed in UI, 3 seconds elapse, count down to 0 begins, count down ends in 2 seconds,
	 * and total test time (from final release of button till count down completion is 5 seconds).
	 */
	public void testActivityScenarioRun() throws Throwable {
		
		mActivity.runOnUiThread(new Runnable() { @Override public void run() {
			assertEquals(0, getDisplayedValue());
			assertEquals(STOPPED_STATE, getState());
			
			for (int c = 0; c < NUM_OF_BUTTON_CLICKS; c++){
				assertTrue(getButton().performClick());
			}
			assertEquals(TIME_SET_STATE, getState());
			assertEquals(EXPECTED_SET_TIME, getDisplayedValue()); 
		}});
		Thread.sleep(DELTA); // <-- AlexTimer "setup" time (time for app to get up to speed)
		while (getDisplayedValue() > 0){ 
			Thread.sleep(1000); 
			actualTestDuration++;		
			
			if (actualTestDuration == IDLE_TIME){
				assertEquals(RUNNING_STATE, getState());
			}
		}
		assertEquals(EXPECTED_TEST_DURATION, actualTestDuration); 
		assertEquals(ALARM_STATE, getState());
		Thread.sleep(10000);  // <-- Time for alarm to play.
	}
	
	/**
	 * Verifies the following scenario: Time is 0, button is pressed 2 times and released, 
	 * 2 is displayed in UI, 3 seconds elapse, count down to 0 begins, button is pressed and 
	 * count down is cancelled, 0 is displayed in the UI. 
	 */
	public void testActivityScenarioCancelRun() throws Throwable { 	 
		
		mActivity.runOnUiThread(new Runnable() { @Override public void run() {
			assertEquals(0, getDisplayedValue());
			assertEquals(STOPPED_STATE, getState());
			
			for (int c = 0; c < NUM_OF_BUTTON_CLICKS; c++){
				assertTrue(getButton().performClick());
			}
			assertEquals(TIME_SET_STATE, getState());
			assertEquals(EXPECTED_SET_TIME, getDisplayedValue()); 
		}});
		Thread.sleep(DELTA); 
		while (getDisplayedValue() > 0 ){ 
			Thread.sleep(1000); 
			actualTestDuration++;
	
			if (actualTestDuration == IDLE_TIME){ // <-- If true, TIME_SET state has just been completed
				mActivity.runOnUiThread(new Runnable() { @Override public void run() {
					assertEquals(RUNNING_STATE, getState()); // Running state should have begun
					assertEquals(EXPECTED_SET_TIME, getDisplayedValue()); // At the beginning of the Running State, UI time should temporarily be what the user set
					assertTrue(getButton().performClick()); // This clicks the button to cancel the count down
					assertEquals(0, getDisplayedValue()); // Upon count down cancellation, displayed UI time should be 0
					assertEquals(STOPPED_STATE, getState()); // State machine should in Stopped state
				}});
			}
		}	
	}
	
	protected String getState() {
		final TextView t = (TextView) mActivity.findViewById(R.id.stateName);
		return t.getText().toString().trim();
	}
	
	protected Button getButton() {
		return (Button) mActivity.findViewById(R.id.timerButton);
	}
	
	protected int getDisplayedValue() {
		final TextView t = (TextView) mActivity.findViewById(R.id.seconds);
		return Integer.parseInt(t.getText().toString().trim());
	}
}
